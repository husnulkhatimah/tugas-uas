import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class soekarno extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>      Soekarno lahir dengan nama Kusno yang diberikan oleh orangtuanya lahir di Surabaya, Jawa Timur, 6 Juni 1901 – meninggal di Jakarta, 21 Juni 1970 pada umur 69 tahun adalah Presiden pertama Republik Indonesia yang menjabat pada periode 1945–1967 Ia adalah seorang tokoh perjuangan yang memainkan peranan penting dalam memerdekakan bangsa Indonesia dari penjajahan Belanda. Ia adalah Proklamator Kemerdekaan Indonesia (bersama dengan Mohammad Hatta) yang terjadi pada tanggal 17 Agustus 1945. Soekarno adalah yang pertama kali mencetuskan konsep mengenai Pancasila sebagai dasar negara Indonesia dan ia sendiri yang menamainya</Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Kesehatan Soekarno sudah mulai menurun sejak bulan Agustus 1965. Sebelumnya, ia telah dinyatakan mengidap gangguan ginjal dan pernah menjalani perawatan di Wina, Austria tahun 1961 dan 1964. Prof. Dr. K. Fellinger dari Fakultas Kedokteran Universitas Wina menyarankan agar ginjal kiri Soekarno diangkat, tetapi ia menolaknya dan lebih memilih pengobatan tradisional. Ia bertahan selama 5 tahun sebelum akhirnya meninggal pada hari Minggu, 21 Juni 1970 di Rumah Sakit Pusat Angkatan Darat (RSPAD) Gatot Subroto, Jakarta dengan status sebagai tahanan politik. Jenazah Soekarno pun dipindahkan dari RSPAD ke Wisma Yasso yang dimiliki oleh Ratna Sari Dewi. </Text>, nomor:2},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Penghargaan</Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Semasa hidupnya, Soekarno mendapatkan gelar Doktor Honoris Causa dari 26 universitas di dalam dan luar negeri. </Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada bulan April 2005, Soekarno yang sudah meninggal selama 35 tahun mendapatkan penghargaan dari Presiden Afrika Selatan Thabo Mbeki. Penghargaan tersebut adalah penghargaan bintang kelas satu The Order of the Supreme Companions of OR Tambo yang diberikan dalam bentuk medali, pin, tongkat, dan lencana yang semuanya dilapisi emas.</Text>, nomor:5},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Karya tulis</Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>1. Sukarno. Pancasila dan Perdamaian Dunia</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>2. Sukarno. Kepada Bangsaku : Karya-karya Bung Karno Pada Tahun 1926-1930-1933-1947-1957.</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>3. Sukarno. Cindy Adams. (1965). Bung Karno: Penyambung Lidah Rakyat Indonesia.</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>4. Sukarno. Pantja Sila Sebagai Dasar Negara.</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>5. Sukarno. Bung Karno Tentang Marhaen Dan Proletar.</Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>6. Sukarno. Negara Nasional Dan Cita-Cita Islam: Kuliah Umum Presiden Soekarno.</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>7. Sukarno. (1933). Mencapai Indonesia Merdeka. </Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>8. Sukarno. (1945). Lahirnya Pancasila</Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>9. Sukarno. (1951). Indonesia Menggugat: Pidato Pembelaan Bung Karno di Depan Pengadilan Kolonial.</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>10. Sukarno. (1951). Sarinah: Kewajiban Wanita Dalam Perjuangan Republik Indonesia.</Text>, nomor:16},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>11. Sukarno. (1957). Indonesia Merdeka.</Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>12. Sukarno. (1959). Dibawah Bendera Revolusi Jilid 1. (kumpulan esai)</Text>, nomor:18},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>13. Sukarno. (1960). Dibawah Bendera Revolusi Jilid 2. (kumpulan esai)</Text>, nomor:19},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>14. Sukarno. (1960). Amanat Penegasan Presiden Soekarno Didepan Sidang Istimewa Depernas Tanggal 9 Djanuari 1960.</Text>, nomor:20},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>15. Sukarno. (1964). Tjamkan Pantja Sila ! : Pantja Sila Dasar Falsafah Negara.</Text>, nomor:21},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>16. Sukarno. (1964). Komando Presiden/Pemimpin Besar Revolusi: Bersiap-sedialah Menerima Tugas untuk Menjelamatkan R.I. dan untuk Mengganjang "Malaysia"!</Text>, nomor:22},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>17. Sukarno. (1965). Wedjangan Revolusi.</Text>, nomor:23},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>18. Sukarno. (1965). Tjapailah Bintang-Bintang di Langit: Tahun Berdikari.</Text>, nomor:24},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>19. Sukarno. (1965). Pantja Azimat Revolusi.</Text>, nomor:25},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Buku</Text>, nomor:26},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>1. M. Yuanda Zara. Ratna Sari Dewi Sukarno.</Text>, nomor:27},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>2. Sukarno, Iman Toto K. Rahardjo (Editor), Herdianto WK (Editor). (2001). Bung Karno dan Wacana Islam: Kenangan 100 tahun Bung Karno.</Text>, nomor:28},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>3. John Beilenson. Sukarno</Text>, nomor:29},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>4. Cindy Adams. Sukarno: My Friend.</Text>, nomor:30},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>5. Adams, C. (2011). Bung Karno Penyambung Lidah Rakyat Indonesia. Penerjemah Syamsu Hadi. Ed. Rev. Yogyakarta: Media Pressindo, dan Yayasan Bung Karno, ISBN 979-911-032-7-9.</Text>, nomor:31},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>6. Guntur Sukarno. Sukarno: Bapakku, Kawanku, Guruku.</Text>, nomor:32},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>7. Peter Polomka. Indonesia Since Sukarno .</Text>, nomor:33},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>8. Clifford Geertz, Benedict Anderson, Wim F. Wertheim. Sukarno di Panggung Sejarah</Text>, nomor:34},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>9. Justus Maria van der Kroef. Indonesia After Sukarno.</Text>, nomor:35},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>10. Peter Kasenda. Sukarno Muda: Biografi Pemikiran 1926–1933.</Text>, nomor:36},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>11. Ayub Ranoh. Kepemimpinan Kharismatis: Tinjauan Teologis-Etis Atas Kepemimpinan Kharismatis Sukarno.</Text>, nomor:37},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>12. Books LLC. Sukarno: Indonesia-Malaysia Confrontation, Transition to the New Order, Mohammad Hatta, Megawati Sukarnoputri, Constitution of Indonesia.</Text>, nomor:38},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>13. Anonim. (1956). Presiden Sukarno di Tiongkok.</Text>, nomor:39},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>14. Maslyn Williams. (1965). Five Journeys from Jakarta: Inside Sukarno's Indonesia.</Text>, nomor:40},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>15. John Hughes. (1967). The End of Sukarno: A Coup That Misfired: A Purge That Ran Wild.</Text>, nomor:41},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>16. Bernhard Dahm. (1969). Sukarno dan Perjuangan Kemerdekaan.</Text>, nomor:42},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>17. John D. Legge (1972) Sukarno: A Political.</Text>, nomor:43},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>18. Christiaan Lambert Maria Penders (1974). The Life and Times of Sukarno.</Text>, nomor:44},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>19. Lambert J. Giebels, 1999, Soekarno. Nederlandsch onderdaan. Biografie 1901–1950. Deel I, uitgeverij Bert Bakker Amsterdam, ISBN 90-351-2114-7</Text>, nomor:45},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>20. Lambert J. Giebels, 2001, Soekarno. President, 1950–1970, Deel II, uitgeverij Bert Bakker Amsterdam, ISBN 90-351-2294-1 geb., ISBN 90-351-2325-5 pbk.</Text>, nomor:46},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>21. Lambert J. Giebels, 2005, De stille genocide: de fatale gebeurtenissen rond de val van de Indonesische president Soekarno, ISBN 90-351-2871-0</Text>, nomor:47},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>22. Rex Mortimer. (1974). Indonesian Communism Under Sukarno: Ideology and Politics, 1959–1965.</Text>, nomor:48},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>23. Bambang S. Widjanarko, Antonie C.A. Dake (Introduction), Rahadi S. Karni (Ed.). (1974). The Devious Dalang: Sukarno and the So-Called Untung-Putsch. </Text>, nomor:49},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>24. Hal Kosut (Ed.). (1976). Indonesia: The Sukarno Years.</Text>, nomor:50},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>25. Franklin B. Weinstein. (1976). Indonesian Foreign Policy and the Dilemma of Dependence: From Sukarno to Soeharto.</Text>, nomor:51},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>26. Masashi Nishihara, Dean Praty R. (Translator). (1976). Sukarno, Ratna Sari Dewi, dan Pampasan Perang: Hubungan Indonesia-Jepang 1951–1966.</Text>, nomor:52},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>27. Ganis Harsono. (1977). Recollections of an Indonesian Diplomat in the Sukarno Era.</Text>, nomor:53},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>28. Fatmawati Sukarno. (1978). Fatmawati: Catatan Kecil Bersama Bung Karno (Book, #1).</Text>, nomor:54},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>29. Guntur Sukarno. (1981). Bung Karno & Kesayangannya.</Text>, nomor:55},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>30. Rosihan Anwar. (1981). Sukarno, Tentara, PKI : Segitiga Kekuasaan sebelum Prahara Politik 1961–1965.</Text>, nomor:56},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>31. Ramadhan Kartahadimadja. (1981). Kuantar ke Gerbang: Kisah Cinta Inggit dengan Sukarno.</Text>, nomor:57},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>32. Marshall Green. (1990). Dari Sukarno ke Soeharto: G30 S-PKI dari Kacamata Seorang Duta Besar.</Text>, nomor:58},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>33. Willem Oltmans. (1995). Mijn vriend Sukarno.</Text>, nomor:59},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>34. John Subritzky. (2000). Confronting Sukarno: British, American, Australian and New Zealand Diplomacy in the Malaysian-Indonesian Confrontation, 1961–65.</Text>, nomor:60},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>35. Angus McIntyre, David Reeve. (2002). Sukarno in Retrospect: Annual Indonesia Lecture Series # 24.</Text>, nomor:61},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>36. Victor M. Fic. (2004). Anatomy of the Jakarata Coup: October 1, 1965: The Collusion with China Which Destroyed the Army Command, President Sukarno and the Communist Party of Indonesia.</Text>, nomor:62},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>37. Antonie C.A. Dake. (2005). Sukarno File: Berkas-berkas Soekarno 1965–1967 – Kronologi Suatu Keruntuhan.</Text>, nomor:63},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>38. Wijanarka. (2006). Sukarno dan Desain Rencana Ibu Kota RI di Palangkaraya.</Text>, nomor:64},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>39. Reni Nuryanti. (2007). Perempuan dalam Hidup Sukarno: Biografi Inggit Garnasih.</Text>, nomor:65},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>40. Reni Nuryanti. (2007). Istri-istri Sukarno.</Text>, nomor:66},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>41. Helen-Louise Hunter. (2007). Sukarno and the Indonesian Coup: The Untold Story. </Text>, nomor:67},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>42. M. Yuanda Zara. (2008). Sakura Di Tengah Prahara: Biografi Ratna Sari Dewi Sukarno.</Text>, nomor:68},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>43. Wawan Tunggul Alam. (2008). Demi Bangsaku: Pertentangan Sukarno vs Hatta.</Text>, nomor:69},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>44. Arifin Suryo Nugroho. (2009). Srihana-Srihani:Biografi Hartini Sukarno.</Text>, nomor:70},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>45. Onghokham. (2009). Sukarno, Orang Kiri, & Revolusi G30S 1965.</Text>, nomor:71},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>46. Rushdy Hoesein. (2010). Terobosan Sukarno Dalam Perundingan Linggarjati.</Text>, nomor:72},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>47. Tim Buku TEMPO. (2010). Sukarno: Paradoks Revolusi Indonesia.</Text>, nomor:73},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>48. Arifin Surya Nugraha. (2010). Fatmawati Sukarno : The First Lady.</Text>, nomor:74},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>49. M. Ridwan Lubis (2010). Sukarno dan Modernisme Islam.</Text>, nomor:75},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>50. Books LLC. (2010). People From Blitar, East Java: Sukarno.</Text>, nomor:76},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>51. Bücher Gruppe. (2010). Nationalheld Indonesiens: Tan Malaka, Liste Indonesischer Nationalhelden, Sukarno, Mohammad Hatta, Abdul Muis, Diponegoro, Iskandar Muda.</Text>, nomor:77},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>52. Hong Liu. (2011). Sukarno, Tiongkok, & Pembentukan Indonesia (1949–1965).</Text>, nomor:78},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>53. Hephaestus Books. (2011). National Heroes Of Indonesia, including: Tuanku Imam Bonjol, Sukarno, Wage Rudolf Supratman, Diponegoro, Mohammad Hatta, Adam Malik, Yos Sudarso, Sudirman, Hamengkubuwono Ix, Sutan Sjahrir, Kartini, Sultan Agung Of Mataram, Abdul Muis, Rizal Nurdin.</Text>, nomor:79},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>54. Peter Kasenda. (2012). Hari – Hari Terakhir Sukarno.</Text>, nomor:80},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>55. Jesse Russell (Editor), Ronald Cohn (Editor). (2012). Rukmini Sukarno.</Text>, nomor:81},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>56. Joseph H. Daves. (2013). The Indonesian Army from Revolusi to Reformasi Volume 1: The Struggle for Independence and the Sukarno Era.   </Text>, nomor:82},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>57. Joseph H Daves. (2013). The Indonesian Army from Revolusi to Reformasi: Volume 1 – The Struggle for Independence and the Sukarno Era.</Text>, nomor:83},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>58. Stefan Seefelder. (2014). Die Bedeutung Der Fruhen Komintern Fur Die Kommunistischen Antikolonialen Bewegungen Asiens. Maos Und Sukarnos.</Text>, nomor:84},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>59. Peter Kasenda. (2014). Sukarno, Marxisme & Leninisme: Akar Pemikiran Kiri & Revolusi Indonesia.</Text>, nomor:85},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>60. Walentina Waluyanti de Jonge. (2015). Sukarno-Hatta Bukan Proklamator Paksaan.</Text>, nomor:86},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>61. Dr. Syafiq A. Mughnie,M.A.,PhD. Hassan Bandung, Pemikir Islam Radikal. PT. Bina Ilmu, 1994, pp 110–111.</Text>, nomor:87},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>62. Leslie H. Palmier. Sukarno, the Nationalist. Pacific Affairs, vol. 30, No, 2 (Jun. 1957), pp 101–119.  </Text>, nomor:88},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>63. Bob Hering, 2001, Soekarno, architect of a nation, 1901–1970, KIT Publishers Amsterdam, ISBN 90-6832-510-8, KITLV Leiden, ISBN 90-6718-178-1</Text>, nomor:89},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>64. Stefan Huebner, Pan-Asian Sports and the Emergence of Modern Asia, 1913–1974.Singapore: NUS Press, 2016, 174-201.</Text>, nomor:90},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Lagu</Text>, nomor:92},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Lagu berjudul "Untuk Paduka Jang Mulia Presiden Soekarno" ditulis pada awal dekade 1960-an oleh Soetedjo dan dipopulerkan oleh Lilis Suryani, solis perempuan terkenal Indonesia era itu. Liriknya penuh dengan puja-puji untuk Presiden seumur hidup tersebut.</Text>, nomor:93},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Film, televisi, dan panggung pertunjukan</Text>, nomor:94},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Di kancah perfilman, hiburan televisi, dan panggung teater Indonesia dan negara lain, ada beberapa aktor yang memerankan sosok Bung Karno. Semua aktor tersebut, tentu saja bermain dalam film dan panggung pertunjukan dan judul yang berbeda. Kebanyakan aktor itu, ketika mendapatkan tawaran main, merasa bangga karena memerankan tokoh besar, pahlawan proklamator, bapak pendiri bangsa, sekaligus presiden pertama Republik Indonesia.</Text>, nomor:95},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>        </Text>, nomor:96},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>        </Text>, nomor:97},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>        </Text>, nomor:98},
                
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>SOEKARNO</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default soekarno;