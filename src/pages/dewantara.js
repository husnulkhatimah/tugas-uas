import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class dewantara extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Raden Mas Soewardi Soerjaningrat ,sejak 1922 menjadi Ki Hadjar Dewantara, Ki Hajar Dewantara, beberapa menuliskan bunyi bahasa Jawanya dengan Ki Hajar Dewantoro, lahir di Pakualaman, 2 Mei 1889 – meninggal di Yogyakarta, 26 April 1959 pada umur 69 tahun;[1] selanjutnya disingkat sebagai "Soewardi" atau "KHD") adalah aktivis pergerakan kemerdekaan Indonesia, kolumnis, politisi, dan pelopor pendidikan bagi kaum pribumi Indonesia dari zaman penjajahan Belanda. Ia adalah pendiri Perguruan Taman Siswa, suatu lembaga pendidikan yang memberikan kesempatan bagi para pribumi untuk bisa memperoleh hak pendidikan seperti halnya para priyayi maupun orang-orang Belanda. </Text>, nomor:1},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Masa Muda dan Awal Karier</Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Soewardi berasal dari lingkungan keluarga Kadipaten Pakualaman, putra dari GPH Soerjaningrat, dan cucu dari Pakualam III. Ia menamatkan pendidikan dasar di ELS (Sekolah Dasar Eropa/Belanda). Kemudian sempat melanjut ke STOVIA (Sekolah Dokter Bumiputera), tetapi tidak sampai tamat karena sakit. Kemudian ia bekerja sebagai penulis dan wartawan di beberapa surat kabar, antara lain, Sediotomo, Midden Java, De Expres, Oetoesan Hindia, Kaoem Moeda, Tjahaja Timoer, dan Poesara. Pada masanya, ia tergolong penulis handal. Tulisan-tulisannya komunikatif dan tajam dengan semangat antikolonial.</Text>, nomor:3},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Aktivitas Pergerakan</Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Selain ulet sebagai seorang wartawan muda, ia juga aktif dalam organisasi sosial dan politik. Sejak berdirinya Boedi Oetomo (BO) tahun 1908, ia aktif di seksi propaganda untuk menyosialisasikan dan menggugah kesadaran masyarakat Indonesia (terutama Jawa) pada waktu itu mengenai pentingnya persatuan dan kesatuan dalam berbangsa dan bernegara. Kongres pertama BO di Yogyakarta juga diorganisasi olehnya.</Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Soewardi muda juga menjadi anggota organisasi Insulinde, suatu organisasi multietnik yang didominasi kaum Indo yang memperjuangkan pemerintahan sendiri di Hindia Belanda, atas pengaruh Ernest Douwes Dekker (DD). Ketika kemudian DD mendirikan Indische Partij, Soewardi diajaknya pula</Text>, nomor:6},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Dalam Oengasingan</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Dalam pengasingan di Belanda, Soewardi aktif dalam organisasi para pelajar asal Indonesia, Indische Vereeniging (Perhimpunan Hindia). Tahun 1913 dia mendirikan Indonesisch Pers-bureau, "kantor berita Indonesia". Ini adalah penggunaan formal pertama dari istilah "Indonesia", yang diciptakan tahun 1850 oleh ahli bahasa asal Inggeris George Windsor Earl dan pakar hukum asal Skotlandia James Richardson Logan.</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Di sinilah ia kemudian merintis cita-citanya memajukan kaum pribumi dengan belajar ilmu pendidikan hingga memperoleh Europeesche Akta, suatu ijazah pendidikan yang bergengsi yang kelak menjadi pijakan dalam mendirikan lembaga pendidikan yang didirikannya. Dalam studinya ini Soewardi terpikat pada ide-ide sejumlah tokoh pendidikan Barat, seperti Froebel dan Montessori, serta pergerakan pendidikan India, Santiniketan, oleh keluarga Tagore. Pengaruh-pengaruh inilah yang mendasarinya dalam mengembangkan sistem pendidikannya sendiri.</Text>, nomor:9},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Pengabdian Pada Masa Indonesia Merdeka</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Tanggal 17 Agustus 1946 ditetapkan sebagai Maha Guru pada Sekolah Polisi Republik Indonesia bagian Tinggi di Mertoyudan Magelang, oleh P.J.M. Presiden Republik Indonesia.</Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Dalam kabinet pertama Republik Indonesia, KHD diangkat menjadi Menteri Pengajaran Indonesia (posnya disebut sebagai Menteri Pendidikan, Pengajaran dan Kebudayaan) yang pertama. Pada tahun 1957 ia mendapat gelar doktor kehormatan (doctor honoris causa, Dr.H.C.) dari universitas tertua Indonesia, Universitas Gadjah Mada. Atas jasa-jasanya dalam merintis pendidikan umum, ia dinyatakan sebagai Bapak Pendidikan Nasional Indonesia dan hari kelahirannya dijadikan Hari Pendidikan Nasional (Surat Keputusan Presiden RI no. 305 tahun 1959, tanggal 28 November 1959).</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Ia meninggal dunia di Yogyakarta tanggal 26 April 1959 dan dimakamkan di Taman Wijaya Brata.</Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:16},
                
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>KI HADJAR DEWANTARA</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default dewantara;