import React, {Component} from "react";
import {StyleSheet,Text,View,TouchableOpacity,Alert,Image,ImageBackground} from "react-native";
import {InputU} from '../../component';
import FIREBASE from '../../config/FIREBASE';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserName: '',
      Password: '',
    };
  }
  onChangeText = (nameState, value) =>{
    this.setState({
      [nameState] : value
    });
  };
  onSubmit = () => {
    if(this.state.UserName && this.state.Password) {
      const loginreferensi = FIREBASE.database().ref('Login');
      const login = {
        username: this.state.UserName,
        password: this.state.Password
      }
      loginreferensi
      .push(login)
      .then((data) => {
        Alert.alert('sukses', 'login berhasil');
        this.props.navigation.replace('Welcome');
      })
      .catch((error) => {
        console.log("Error : ", error);
      })
    }else{
      Alert.alert('Error', 'Username dan Password wajib di isi');
    }
  };
 render (){
    return (
    
      <View style={styles.container}> 
      <ImageBackground source={require('../../gambar/bendera.jpg')} style={{width:500,height:900}}>
        <View style={styles.page}>
        <Text style={{fontSize:50,marginBottom:30}}>INDONESIAKU</Text>
        <InputU
        placeholder={'UserName'}
        keyType="email-address"
        onChangeText={this.onChangeText}
        value={this.state.UserName}
        nameState='UserName'
        />
        <InputU 
        
        placeholder={'Password'}
        onChangeText={this.onChangeText}
        value={this.state.Password}
        nameState='Password'
        />
        <TouchableOpacity style={{borderColor:'white',
        width:100,height:50,
        paddingHorizontal:10,
        borderRadius:15,
        marginTop:15,
        backgroundColor:'white',
        color:'black'
        }} onPress={() => this.onSubmit()}>
          <Text style={{textAlign:'center',fontSize:20,color:'black',marginTop:15}}>Login</Text>
        </TouchableOpacity>
        </View>
        
      </ImageBackground>
      </View>
    )
  }
}
export default Login;

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems:"center",
    justifyContent:"center",
    },
  inputText:{
    height: 50,
    color:'white'
},
page:{
  width: 380,height:470,
  borderRadius: 3,
  justifyContent:'center',
  alignItems:'center',
  marginStart:50,
  marginTop:200
},
}); 
