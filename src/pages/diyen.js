import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class diyen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Tjoet Nja' Dhien, Lampadang, Kerajaan Aceh, 1848 – Sumedang, Jawa Barat, 6 November 1908,  dimakamkan di Gunung Puyuh, Sumedang) adalah seorang Pahlawan Nasional Indonesia dari Aceh yang berjuang melawan Belanda pada masa Perang Aceh. Setelah wilayah VI Mukim diserang, ia mengungsi, sementara suaminya Ibrahim Lamnga bertempur melawan Belanda. Tewasnya Ibrahim Lamnga di Gle Tarum pada tanggal 29 Juni 1878 kemudian menyeret Cut Nyak Dhien lebih jauh dalam perlawanannya terhadap Belanda.</Text>, nomor:1},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Kehidupan Awal </Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Cut Nyak Dhien dilahirkan dari keluarga bangsawan yang taat beragama di Aceh Besar, wilayah VI Mukim pada tahun 1848. Ayahnya bernama Teuku Nanta Seutia, seorang uleebalang VI Mukim, yang juga merupakan keturunan Datuk Makhudum Sati, perantau dari MinangkabauSedangkan ibunya merupakan putri uleebalang Lampageu.</Text>, nomor:3},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Perlawanan saat Perang Aceh</Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Rencong merupakan senjata tradisional milik Suku Aceh. Cut Nyak Dhien menggunakan Rencong sebagai salah satu alat perang untuk melawan para tentara Kerajaan Belanda pada saat Kerajaan Belanda menyerang Kerajaan Aceh dan membakar Masjid Raya Baiturrahman pada tahun 1873.</Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada tanggal 26 Maret 1873, Belanda menyatakan perang kepada Aceh, dan mulai melepaskan tembakan meriam ke daratan Aceh dari kapal perang Citadel van Antwerpen. Perang Aceh pun meletus. Pada perang pertama (1873-1874), Aceh yang dipimpin oleh Panglima Polim dan Sultan Machmud Syah bertempur melawan Belanda yang dipimpin Johan Harmen Rudolf Köhler. Saat itu, Belanda mengirim 3.198 prajurit. Lalu, pada tanggal 8 April 1873, Belanda mendarat di Pantai Ceureumen di bawah pimpinan Köhler, dan langsung bisa menguasai Masjid Raya Baiturrahman dan membakarnya. Kesultanan Aceh dapat memenangkan perang pertama. Ibrahim Lamnga yang bertarung di garis depan kembali dengan sorak kemenangan, sementara Köhler tewas tertembak pada April 1873.</Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada tahun 1874-1880, di bawah pimpinan Jenderal Jan van Swieten, daerah VI Mukim dapat diduduki Belanda pada tahun 1873, sedangkan Keraton Sultan jatuh pada tahun 1874. Cut Nyak Dhien dan bayinya akhirnya mengungsi bersama ibu-ibu dan rombongan lainnya pada tanggal 24 Desember 1875. Suaminya selanjutnya bertempur untuk merebut kembali daerah VI Mukim.</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Ketika Ibrahim Lamnga bertempur di Gle Tarum, ia tewas pada tanggal 29 Juni 1878. Hal ini membuat Cut Nyak Dhien sangat marah dan bersumpah akan menghancurkan Belanda. </Text>, nomor:8},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Masa Tua dan Kematian </Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Setelah ditangkap, Cut Nyak Dhien dibawa ke Banda Aceh dan dirawat di situ. Penyakitnya seperti rabun dan encok berangsur-angsur sembuh. Namun, Cut Nyak Dien akhirnya dibuang ke Sumedang, Jawa Barat, karena ketakutan Belanda bahwa kehadirannya akan menciptakan semangat perlawanan dan juga karena ia terus berhubungan dengan pejuang yang belum tunduk.</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Ia dibawa ke Sumedang bersama dengan tahanan politik Aceh lain dan menarik perhatian bupati Suriaatmaja. Selain itu, tahanan laki-laki juga menyatakan perhatian mereka pada Cut Nyak Dhien, tetapi tentara Belanda dilarang mengungkapan identitas tahanan. Ia ditahan bersama ulama bernama Ilyas yang segera menyadari bahwa Cut Nyak Dhien merupakan ahli dalam agama Islam, sehingga ia dijuluki sebagai "Ibu Perbu".</Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada tanggal 6 November 1908, Cut Nyak Dhien meninggal karena usianya yang sudah tua. Makam "Ibu Perbu" baru ditemukan pada tahun 1959 berdasarkan permintaan Gubernur Aceh saat itu, Ali Hasan. "Ibu Perbu" diakui oleh Presiden Soekarno sebagai Pahlawan Nasional Indonesia melalui SK Presiden RI No.106 Tahun 1964 pada tanggal 2 Mei 1964.</Text>, nomor:12},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Pengabadian </Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>1. Sebuah kapal perang TNI-AL diberi nama KRI Cut Nyak Dhien.</Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>2. Mata uang rupiah yang bernilai sebesar Rp10.000,00 yang dikeluarkan tahun 1998 memuat gambar Cut Nyak Dhien dengan deskripsi Tjoet Njak Dhien.</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>3. Namanya diabadikan di berbagai kota Indonesia sebagai nama jalan.</Text>, nomor:16},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>4. Masjid Aceh kecil didirikan di dekat makamnya untuk mengenangnya.</Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>5. Masjid Cut Nyak Dien, Jakarta.</Text>, nomor:18},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>6. Museum Rumah Cut Nyak Dhien, Banda Aceh. </Text>, nomor:19},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:20},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:21},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:22},
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>TJOET NYAK DIYEN</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default diyen;