import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class hasim extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Kiai Haji Mohammad Hasjim Asy'arie lahir di Kabupaten Jombang, Jawa Timur, 14 Februari 1871 – meninggal di Jombang, Jawa Timur, 21 Juli 1947 pada umur 76 tahun; 24 Dzul Qo'dah 1287 H- 7 Ramadhan 1366 H; dimakamkan di Tebu Ireng, Jombang adalah salah seorang Pahlawan Nasional Indonesia[3] yang merupakan pendiri Nahdlatul Ulama, organisasi massa Islam yang terbesar di Indonesia. Di kalangan Nahdliyin dan ulama pesantren ia dijuluki dengan sebutan Hadratus Syeikh yang berarti maha guru.</Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>K.H Hasjim Asy'ari adalah putra ketiga dari 10 bersaudara. Ayahnya bernama Kyai Asy'ari, pemimpin Pondok Pesantren yang berada di sebelah selatan Jombang. Ibunya bernama Halimah. Sementara kesepuluh saudaranya antara lain: Nafi'ah, Ahmad Saleh, Radiah, Hassan, Anis, Fatanah, Maimunah, Maksum, Nahrawi dan Adnan. Berdasarkan silsilah garis keturunan ibu, K.H. Hasjim Asy'ari memiliki garis keturunan baik dari Sultan Pajang Jaka Tingkir juga mempunyai keturunan ke raja Hindu Majapahit, Raja Brawijaya V (Lembupeteng). Berikut silsilah berdasarkan K.H. Hasjim Asy'ari berdasarkan garis keturanan ibu.</Text>, nomor:2},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Perjuangan</Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada tahun 1899, sepulangnya dari Mekah, K.H. Hasjim Asy'ari mendirikan Pesantren Tebu Ireng, yang kelak menjadi pesantren terbesar dan terpenting di Jawa pada abad 20.</Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada tahun 1926, K.H Hasjim Asy'ari menjadi salah satu pemrakarsa berdirinya Nadhlatul Ulama (NU), yang berarti kebangkitan ulama.</Text>, nomor:5},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Karya dan Pemikiran </Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pemikiran K.H. Hasjim Asy'ari tentang Ahl al-Sunnah wa al-Jama'ah adalah "ulama dalam bidang tafsir Al-Qur'an, sunnah Rasul, dan fiqh yang tunduk pada tradisi Rasul dan Khulafaur Rasyidin." beliau selanjutnya menyatakan bahwa sampai sekarang ulama tersebut termasuk "mereka yang mengikuti mazhab Maliki, Hanafi, Syafi'i, dan Hambali." Doktrin ini diterapkan dalam NU yang menyatakan sebagai pengikut, penjaga dan penyebar faham Ahl al-Sunnah wa al-Jama'ah.</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Muslim tradisionalis juga menggunakan istilah Ahl al-Sunnah wa al-Jamaah untuk membedakan dirinya dengan Muslim modernis, walaupun yang terakhir ini juga menerima formulasi al-Ash'ari dan al-Maturidi dalam bidang teologi. Namun, tidak seperti kaum modernis, Muslim tradisionalis mengikuti salah satu empat mazhab sunni dan mengakui keabsahan sufi ortodoks sebagaimana yang diajarkan oleh Junaid al-Baghdadi dan al-Ghazali.</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Ahl al-sunnah wa al-jama'ah dalam pandangan K.H. Hasjim Asy'ari tidak memiliki makna tunggal, tergantung perspektif yang digunakan. Paling tidak terdapat dua perspektif yang digunakan untuk mendefinisikan Ahl al-sunnah wa al-jama'ah, yaitu teologi dan fiqh. Namun, jika ditelusuri lebih lanjut melalui karya-karya K.H. Hasjim Asy'ari, maka sebenarnya dapat diambil sebuah kesimpulan yaitu Ahl al-sunnah wa al-jama'ah pada dasarnya lebih mengandaikan pola keberagaman bermadzhab kepada generasi Muslim masa lalu yang cukup otoritatif secara religious. </Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>K.H. Hasjim Asy'ari banyak membuat tulisan dan catatan-catatan. Sekian banyak dari pemikirannya, setidaknya ada empat kitab karangannya yang mendasar dan menggambarkan pemikirannya; kitab-kitab tersebut antara lain:</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>a. Risalah Ahlis-Sunnah Wal Jama'ah: Fi Hadistil Mawta wa Asyrathis-sa'ah wa baya Mafhumis-Sunnah wal Bid'ah (Paradigma Ahlussunah wal Jama'ah: Pembahasan tentang Orang-orang Mati, Tanda-tanda Zaman, dan Penjelasan tentang Sunnah dan Bid'ah).</Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>b. Al-Nuurul Mubiin fi Mahabbati Sayyid al-Mursaliin (Cahaya yang Terang tentang Kecintaan pada Utusan Tuhan, Muhammad SAW).</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>c. Adab al-alim wal Muta'allim fi maa yahtaju Ilayh al-Muta'allim fi Ahwali Ta'alumihi wa maa Ta'limihi (Etika Pengajar dan Pelajar dalam Hal-hal yang Perlu Diperhatikan oleh Pelajar Selama Belajar).</Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>d. Al-Tibyan: fin Nahyi 'an Muqota'atil Arham wal Aqoorib wal Ikhwan (Penjelasan tentang Larangan Memutus Tali Silaturrahmi, Tali Persaudaraan dan Tali Persahabatan)</Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>e. Muqaddimah al-Qanun al-Asasi li Jam’iyyat Nahdlatul Ulama. Dari kitab ini para pembaca akan mendapat gambaran bagaimana pemikiran dasar dia tentang NU. Di dalamnya terdapat ayat dan hadits serta pesan penting yang menjadi landasan awal pendirian jam’iyah NU. Boleh dikata, kitab ini menjadi “bacaan wajib” bagi para pegiat NU.</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>f. Risalah fi Ta’kid al-Akhdzi bi Mazhab al-A’immah al-Arba’ah. Mengikuti manhaj para imam empat yakni Imam Syafii, Imam Malik, Imam Abu Hanifah dan Imam Ahmad bin Hanbal, tentunya memiliki makna khusus sehingga akhirnya mengikuti jejak pendapat imam empat tersebut dapat ditemukan jawabannya dalam kitab ini.</Text>, nomor:16},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>g. Mawaidz. Adalah kitab yang bisa menjadi solusi cerdas bagi para pegiat di masyarakat. Saat Kongres NU XI tahun 1935 di Bandung, kitab ini pernah diterbitkan secara massal. Demikian juga Prof Buya Hamka harus menterjemah kitab ini untuk diterbitkan di majalah Panji Masyarakat, edisi 15 Agustus 1959.</Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>h. Arba’ina Haditsan Tata’allaqu bi Mabadi’ Jam’iyyat Nahdlatul Ulama. Hidup ini tak akan lepas dari rintangan dan tantangan. Hanya pribadi yang tangguh serta memiliki sosok yang kukuh dalam memegang prinsiplah yang akan lulus sebagai pememang. Kitab ini berisikan 40 hadits pilihan yang seharusnya menjadi pedoman bagi warga NU.</Text>, nomor:18},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>i. Al-Tanbihat al-Wajibat liman Yushna’ al-Maulid bi al-Munkarat. Kitab ini menyajikan beberapa hal yang harus diperhatikan saat memperingati maulidur rasul.</Text>, nomor:19},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:20},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:21},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:22},
               
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>HASYIM ASYARI</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}

export default hasim;