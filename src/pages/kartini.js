import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class kartini extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Raden Adjeng Kartini (lahir di Jepara, Hindia Belanda, 21 April 1879 – meninggal di Rembang, Hindia Belanda, 17 September 1904 pada umur 25 tahun) atau sebenarnya lebih tepat disebut Raden Ayu Kartin adalah seorang tokoh Jawa dan Pahlawan Nasional Indonesia. Kartini dikenal sebagai pelopor kebangkitan perempuan Pribumi-Nusantara.</Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Raden Adjeng Kartini berasal dari kalangan priyayi atau kelas bangsawan Jawa. Ia merupakan putri dari Raden Mas Adipati Ario Sosroningrat, seorang patih yang diangkat menjadi bupati Jepara segera setelah Kartini lahir. Kartini adalah putri dari istri pertama, tetapi bukan istri utama. Ibunya bernama M.A. Ngasirah, putri dari Nyai Haji Siti Aminah dan Kyai Haji Madirono, seorang guru agama di Telukawur, Jepara.</Text>, nomor:2},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Surat-surat </Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Setelah Kartini wafat, Jacques Abendanon mengumpulkan dan membukukan surat-surat yang pernah dikirimkan R.A Kartini pada teman-temannya di Eropa. Abendanon saat itu menjabat sebagai Menteri Kebudayaan, Agama, dan Kerajinan Hindia Belanda. Buku itu diberi judul Door Duisternis tot Licht yang arti harfiahnya "Dari Kegelapan Menuju Cahaya". Buku kumpulan surat Kartini ini diterbitkan pada 1911. Buku ini dicetak sebanyak lima kali, dan pada cetakan terakhir terdapat tambahan surat Kartini.</Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada tahun 1922, Balai Pustaka menerbitkannya dalam bahasa Melayu dengan judul yang diterjemahkan menjadi Habis Gelap Terbitlah Terang: Boeah Pikiran, yang merupakan terjemahan oleh Empat Saudara. Kemudian tahun 1938, keluarlah Habis Gelap Terbitlah Terang versi Armijn Pane seorang sastrawan Pujangga Baru. Armijn membagi buku menjadi lima bab pembahasan untuk menunjukkan perubahan cara berpikir Kartini sepanjang waktu korespondensinya. Versi ini sempat dicetak sebanyak sebelas kali. Surat-surat Kartini dalam bahasa Inggris juga pernah diterjemahkan oleh Agnes L. Symmers. Selain itu, surat-surat Kartini juga pernah diterjemahkan ke dalam bahasa-bahasa Jawa dan Sunda.</Text>, nomor:5},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Pemikiran </Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada surat-surat Kartini tertulis pemikiran-pemikirannya tentang kondisi sosial saat itu, terutama tentang kondisi perempuan pribumi. Sebagian besar surat-suratnya berisi keluhan dan gugatan khususnya menyangkut budaya di Jawa yang dipandang sebagai penghambat kemajuan perempuan. Dia ingin wanita memiliki kebebasan menuntut ilmu dan belajar. Kartini menulis ide dan cita-citanya, seperti tertulis: Zelf-ontwikkeling dan Zelf-onderricht, Zelf- vertrouwen dan Zelf-werkzaamheid dan juga Solidariteit. Semua itu atas dasar Religieusiteit, Wijsheid en Schoonheid (yaitu Ketuhanan, Kebijaksanaan dan Keindahan), ditambah dengan Humanitarianisme (peri kemanusiaan) dan Nasionalisme (cinta tanah air).</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Surat-surat Kartini juga berisi harapannya untuk memperoleh pertolongan dari luar.</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Surat-surat Kartini banyak mengungkap tentang kendala-kendala yang harus dihadapi ketika bercita-cita menjadi perempuan Jawa yang lebih maju.</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Keinginan Kartini untuk melanjutkan studi, terutama ke Eropa, memang terungkap dalam surat-suratnya.</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada pertengahan tahun 1903 saat berusia sekitar 24 tahun, niat untuk melanjutkan studi menjadi guru di Betawi pun pupus. Dalam sebuah surat kepada Nyonya Abendanon, Kartini mengungkap tidak berniat lagi karena ia sudah akan menikah. "...Singkat dan pendek saja, bahwa saya tiada hendak mempergunakan kesempatan itu lagi, karena saya sudah akan kawin..." Padahal saat itu pihak departemen pengajaran Belanda sudah membuka pintu kesempatan bagi Kartini dan Rukmini untuk belajar di Betawi.</Text>, nomor:11},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Buku</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>1. Habis Gelap Terbitlah Terang</Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>2. Surat-surat Kartini, Renungan Tentang dan Untuk Bangsanya</Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>3. Letters from Kartini, An Indonesian Feminist 1900–1904</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>4. Panggil Aku Kartini Saja</Text>, nomor:16},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>5. Kartini Surat-surat kepada Ny RM Abendanon-Mandri dan suaminya</Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>6. Aku Mau Feminisme dan Nasionalisme.</Text>, nomor:18},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>7. Surat-surat Kartini kepada Stella Zeehandelaar 1899-1903</Text>, nomor:19},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Peringatan</Text>, nomor:20},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>1. Hari Kartini</Text>, nomor:21},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>Presiden Soekarno mengeluarkan Keputusan Presiden Republik Indonesia Nomor 108 Tahun 1964, tanggal 2 Mei 1964, yang menetapkan Kartini sebagai Pahlawan Kemerdekaan Nasional sekaligus menetapkan hari lahir Kartini, tanggal 21 April, untuk diperingati setiap tahun sebagai hari besar yang kemudian dikenal sebagai Hari Kartini.</Text>, nomor:22},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>2. Nama jalan di Belanda</Text>, nomor:23},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>a. Utrecht</Text>, nomor:24},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>b. Venlo</Text>, nomor:25},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>c. Amsterdam</Text>, nomor:26},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>d. Haarlem</Text>, nomor:27},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Dalam budaya populer</Text>, nomor:28},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Film R.A. Kartini (1982)</Text>, nomor:29},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>1. Film Surat Cinta untuk Kartini 2016</Text>, nomor:30},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>2. Film Kartini 2017</Text>, nomor:31},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:32},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:33},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:34},
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>R.A KARTINI</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default kartini;