import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class agus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     H. Agus Salim lahir dengan nama Masyhudul Haq (berarti "pembela kebenaran"); lahir di Koto Gadang, Agam, Sumatra Barat, Hindia Belanda, 8 Oktober 1884 – meninggal di Jakarta, Indonesia, 4 November 1954 pada umur 70 tahun) adalah seorang pejuang kemerdekaan Indonesia. Haji Agus Salim ditetapkan sebagai salah satu Pahlawan Nasional Indonesia pada tanggal 27 Desember 1961 melalui Keppres nomor 657 tahun 1961.</Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Agus Salim lahir dari pasangan Soetan Salim gelar Soetan Mohamad Salim dan Siti Zainab. Jabatan terakhir ayahnya adalah Jaksa Kepala di Pengadilan Tinggi Riau. </Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Pendidikan dasar ditempuh di Europeesche Lagere School. Setelah lulus, Salim bekerja sebagai penerjemah dan pembantu notaris pada sebuah kongsi pertambangan di Indragiri. Pada tahun 1906, Salim berangkat ke Jeddah, Arab Saudi untuk bekerja di Konsulat Belanda di sana. Pada periode inilah Salim berguru pada Syeh Ahmad Khatib, yang masih merupakan pamannya.</Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Salim kemudian terjun ke dunia jurnalistik sejak tahun 1915 di Harian Neratja sebagai Redaktur II. Setelah itu diangkat menjadi Ketua Redaksi. Menikah dengan Zaenatun Nahar dan dikaruniai 8 orang anak. Kegiatannya dalam bidang jurnalistik terus berlangsung hingga akhirnya menjadi Pemimpin Harian Hindia Baroe di Jakarta. Kemudian mendirikan Suratkabar Fadjar Asia. Dan selanjutnya sebagai Redaktur Harian Moestika di Yogyakarta dan membuka kantor Advies en Informatie Bureau Penerangan Oemoem (AIPO). Bersamaan dengan itu Agus Salim terjun dalam dunia politik sebagai pemimpin Sarekat Islam.</Text>, nomor:4},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Karya Tulis </Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>1. Riwayat Kedatangan Islam di Indonesia</Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>2. Dari Hal Ilmu Quran</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>3. Muhammad voor en na de Hijrah</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>4. Gods Laatste Boodschap</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>5. Jejak Langkah Haji Agus Salim (Kumpulan karya Agus Salim yang dikompilasi koleganya, Oktober 1954</Text>, nomor:10},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Karya Terjemahan </Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>1. Menjinakkan Perempuan Garang (dari The Taming of the Shrew karya Shakespeare)</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>2. Cerita Mowgli Anak Didikan Rimba (dari The Jungle Book karya Rudyard Kipling)</Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>3. Sejarah Dunia (karya E. Molt)</Text>, nomor:14},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Karier Politik </Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada tahun 1915, H. Agus Salim bergabung dengan Sarekat Islam (SI), dan menjadi pemimpin kedua di SI setelah H.O.S. Tjokroaminoto.</Text>, nomor:16},
                {teks: <Text style={{marginTop:3,fontSize:20,textAlign:'justify',marginLeft:5}}>Peran H. Agus Salim pada masa perjuangan kemerdekaan RI antara lain:</Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:15}}>1. anggota Volksraad (1921-1924)</Text>, nomor:18},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:15}}>2. anggota panitia 9 BPUPKI yang mempersiapkan UUD 1945</Text>, nomor:19},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:15}}>3. Menteri Muda Luar Negeri Kabinet Sjahrir II 1946 dan Kabinet III 1947</Text>, nomor:20},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:15}}>4. pembukaan hubungan diplomatik Indonesia dengan negara-negara Arab, terutama Mesir pada tahun 1947</Text>, nomor:21},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:15}}>5. Menteri Luar Negeri Kabinet Amir Sjarifuddin 1947</Text>, nomor:22},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:15}}>6. Menteri Luar Negeri Kabinet Amir Sjarifuddin 1947</Text>, nomor:23},
                {teks: <Text style={{marginTop:5,fontSize:20,textAlign:'justify'}}>Di antara tahun 1946-1950 ia laksana bintang cemerlang dalam pergolakan politik Indonesia, sehingga kerap kali digelari "Orang Tua Besar" (The Grand Old Man). Ia pun pernah menjabat Menteri Luar Negeri RI pada kabinet Presidentil dan pada tahun 1950 sampai akhir hayatnya dipercaya sebagai Penasehat Menteri Luar Negeri.</Text>, nomor:24},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Pada tahun 1952, ia menjabat Ketua di Dewan Kehormatan PWI</Text>, nomor:25},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Setelah mengundurkan diri dari dunia politik, pada tahun 1953 ia mengarang buku dengan judul Bagaimana Takdir, Tawakal dan Tauchid harus dipahamkan? yang lalu diperbaiki menjadi Keterangan Filsafat Tentang Tauchid, Takdir dan Tawakal.</Text>, nomor:26},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Ia meninggal dunia pada 4 November 1954 di RSU Jakarta dan dimakamkan di TMP Kalibata, Jakarta. Namanya kini diabadikan untuk stadion sepak bola di Padang.</Text>, nomor:27},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:28},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:29},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:30},
                
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>AGUS SALIM</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default agus;