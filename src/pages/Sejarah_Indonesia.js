import React, { Component } from 'react';
import { View, Text, ScrollView,Image,FlatList } from 'react-native';

class pahlawan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Kemerdekaan Indonesia merupakan salah satu sejarah di bangsa Indonesia. Sejarah ini mengandung makna yang sangat besar karena mengorbankan banyak pahlawan demi memperjuangkan kemerdekaan</Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Banyaknya pahlawan yang menjadi korban tersebut, patut menjadi motivasi kita untuk meningkatkan rasa nasionalisme kepada Indonesia. Akan tetapi di era saat ini, telah banyak yang melupakan perjuangan para pahlawan yang telah mengorbankan dirinya demi kemerdekaan bangsan Indonesia.</Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Sejarah secara singkat dapat dilihat pada tahun 1945 hingga 1955. Kemerdekaan Indonesia tentunya diawali dengan banyak kesengsaraan yang harus dialami bangsa Indonesia.  Salah satu penderitaan bangsa Indonesia yang dialami ketika dijajah oleh bangsa Portugis. Bangsa Portugis inilah yang mengawali penjajahan di bangsa Indonesia.</Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Dari keberhasilan bangsa portugis inilah, mendorong bangsa Eropa lainnya untuk melakukan penjajahan kepada bangsa Indonesia.</Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Setelah berjuang untuk melepaskan diri dari kedua negara penjajah tersebut, selanjutnya bangsa Indonesia dihadapkan dengan kehadiran bangsa Belanda dan bangsa Jepang. Selayaknya pada negara-negara sebelumnya, Belanda dan Jepang datang ke Indonesia untuk menjajah bangsa Indonesia.</Text>, nomor:5},
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Peristiwa penting dalam memperjuangkan kemerdekaan Indonesia. Sejarah kemerdekaan bangsa Indonesia tentunya didapatkan usaha yang keras dan penuh perjuangan. Terdapat beberapa peristiwa yang mendorong kemerdekaan. Peristiwa penting tersebut meliputi:</Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>1.	Penyerahan Jepang kepada Sekutu</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Penyerahan jepang kepada sekutu terjadi pada tanggal 14 Agustus 1945. Penyerahan ini dilakukan setelah pada tanggal 6 dan 9 bulan Agustus 1945 pada dua kota industri di Jepang yaitu Hiroshima dan Nagasaki dibom atom oleh Amerika Serikat.</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Selain kejadian pengeboman tersebut, pada tanggal 9 Agustus 1945, Uni Soviet melakukan penyerbuan secara tiba-tiba ke Jepang di Manchuria yang mengingkari kesepakatan Netralitas Jepang dan soviet. Kaisar Hirohito ikut turun tangan pasca terjadinya peristiwa pengeboman dua kota tersebut. Selanjutnya menginstruksikan Dewan Penasihat Militer guna menerima persyaratan yang diajukan sekutu dalam Deklarasi Potsdam.</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Pasca terjadinya pembahasan dan diskusi selama beberapa waktu dan perencanaan kudeta yang gagal, Hirohito memberikan pengumuman melalui radio di depan seluruh rakyat pada 15 Agustus 1945. Pada pidato tersebut, Hirohito menyampaikan tentang perjanjian penyerahan kekuasaan, serta penyerahan diri Jepang kepada Sekutu.</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Selain itu dari kejadian pengeboman di Hiroshima dan Nagasaki, tersiar informasi mengenai kemerdekaan bangsa Indonesia yang akan diserahkan oleh pemerintah Jepang sesegera mungkin.</Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>2.	Peristiwa Pengasingan Rengasdengklok</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Kabar mengenai Jepang mengakui kekalahannya kepada sekutu mengakibatkan beberapa golongan muda seperti Chaerul Saleh, Sutan Sjahrir, Wikanan maupun Darwis mendesak golongan tua agar mempercepat proklamasi kemerdekaan bangsa Indonesia.</Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Akan tetapi para tokoh golongan tua seperti Soekarno dan Hatta belum menyetujui akan permintaan tersebut. Mereka berargumen bahwa memproklamasikan kemerdekaan Indonesia secara mendadak berdampak pada pertumpahan darahan antara pemerintahan Jepang yang belum keseluruhan diduduki oleh Indonesia.</Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Penolakan golongan Tua inilah yang memicu adanya peristiwa Rengasdengklok. Hal ini sebabkan perdebatan antara golongan muda dan tua, sebuah kesepakatan pun akhirnya dikeluarkan. Pada peristiwa ini, sebagian golongan muda mengirim bapak Soekarno dan Hatta ke daerah Rengasdengklok. Tujuan mereka untuk mengamankan Soekarno dan Hatta dari pengaruh pemerintah Jepang.</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>3.	Penyusunan Teks Deklarasi Proklamasi Kemerdekaan</Text>, nomor:16},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Perumusan teks proklamasi menjadi bagian terpenting sejarahnya. Dengan mengadakan pertemuan untuk merumuskan draft proklamasi pada rumah Laksamana Maeda pada 16 Agustus 1945. Pada pertemuan ini dihadiri oleh kelompok golongan muda yang sangat menginginkan kemerdekaan bangsa Indonesia.</Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Dengan jabatan Kepala Kantor Penghubung Angkatan Laut Jepang, kediaman Laksamana Maeda dirasa menjadi lokasi yang tepat dalam menyusun teks proklamasi Indonesia.</Text>, nomor:18},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Hal ini dikarenakan Laksamana Maeda adalah teman baik dari Ahmad Soebardjo yang merupakan salah satu anggota dari golongan tua yang merumuskan teks proklamasi Indonesia. Adanya peristiwa Rengasdengklok, membuat Soekarno dan Moh. Hatta tergerak untuk memproklamasikan kemerdekaan secepatnya.</Text>, nomor:19},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>4.	Pengesahan Teks Proklamasi</Text>, nomor:20},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Pasca penyusunan teks proklamasi yang diadakan oleh Soekarno, Hatta, Soebardjo dan Ahmad terciptalah sebuah teks Proklamasi Kemerdekaan Indonesia yang langsung ditulis oleh bapak Soekarno. Setelah mendapatkan persetujuan dari peserta rapat, dan dilakukan beberapa revisi, selanjutnya Soekarno mengesahkan teks proklamasi di depan semua pihak.</Text>, nomor:21},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>5.   Deklarasi Teks Proklamasi</Text>, nomor:22},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:13}}>Pendeklarasian tentang teks proklamasi kemerdekaan Indonesia dibacakan oleh Ir Soekarno pada tanggal 17 Agustus 1945. Upacara pendeklarasian proklamasi tersebut membuktikan akan kemerdekaan Indonesia yang sesungguhnya.</Text>, nomor:23},
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     </Text>, nomor:24},
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     </Text>, nomor:25},
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>SEJARAH INDONESIA</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default pahlawan;