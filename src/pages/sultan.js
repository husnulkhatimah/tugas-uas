import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class sultan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Sutan Sjahrir  Padang Panjang, Sumatra Barat, 5 Maret 1909, meninggal di Zürich, Swiss, 9 April 1966 pada umur 57 tahun) adalah seorang intelektual, perintis, dan revolusioner kemerdekaan Indonesia. Setelah Indonesia merdeka, ia menjadi politikus dan perdana menteri pertama Indonesia. Ia menjabat sebagai Perdana Menteri Indonesia dari 14 November 1945 hingga 20 Juni 1947. </Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Sjahrir mendirikan Partai Sosialis Indonesia pada tahun 1948. Ia meninggal dalam pengasingan sebagai tawanan politik dan dimakamkan di TMP Kalibata, Jakarta. Sutan Sjahrir ditetapkan sebagai salah seorang Pahlawan Nasional Indonesia pada tanggal 9 April 1966 melalui Keppres nomor 76 tahun 1966.</Text>, nomor:2},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Partai Sosialis Indonesia</Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Meskipun perawakannya kecil, yang oleh teman-temannya sering dijuluki Si Kancil, Sutan Sjahrir adalah salah satu penggemar olahraga dirgantara, pernah menerbangkan pesawat kecil dari Jakarta ke Yogyakarta pada kesempatan kunjungan ke Yogyakarta. Di samping itu juga senang sekali dengan musik klasik. Ia juga bisa memainkan biola.</Text>, nomor:4},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Akhir hidup</Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Tahun 1955 PSI gagal mengumpulkan suara dalam pemilihan umum pertama di Indonesia. Setelah kasus PRRI tahun 1958, hubungan Sutan Sjahrir dan Presiden Soekarno memburuk sampai akhirnya PSI dibubarkan tahun 1960. Tahun 1962 hingga 1965, Sjahrir ditangkap dan dipenjarakan tanpa diadili sampai menderita stroke. Setelah itu Sjahrir diizinkan untuk berobat ke Zürich Swiss. Salah seorang kawan dekat yang pernah menjabat wakil ketua PSI Sugondo Djojopuspito mengantarkannya ke Bandara Kemayoran dan Sjahrir memeluk Sugondo dengan air mata. Sjahrir akhirnya meninggal di Swiss pada tanggal 9 April 1966.</Text>, nomor:6},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Karya</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pikiran dan Perjuangan, tahun 1950 (kumpulan karangan dari Majalah ”Daulat Rakyat” dan majalah-majalah lain, tahun 1931 – 1940)</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>1.	Pergerakan Sekerja, tahun 1933</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>2.	Perjuangan Kita, tahun 1945</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>3.	Indonesische Overpeinzingen, tahun 1946 (kumpulan surat-surat dan karangan-karangan dari penjara Cipinang dan tempat pembuangan di Digul dan Banda-Neira, dari tahun 1934 sampau 1938). (Versi digital dan dbnl)</Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>4.	Renungan Indonesia, tahun 1951 (diterjemahkan dari Bahasa Belanda: Indonesische Overpeinzingen oleh HB Yassin)</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>5.	Out of Exile, tahun 1949 (terjemahan dari ”Indonesische Overpeinzingen” oleh Charles Wolf Jr. dengan dibubuhi bagian ke-2 karangan Sutan Sjahrir)</Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>6.	Renungan dan Perjuangan, tahun 1990 (terjemahan HB Yassin dari Indonesische Overpeinzingen dan Bagian II Out of Exile)</Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>7.	Sosialisme dan Marxisme, tahun 1967 (kumpulan karangan dari majalah “Suara Sosialis” tahun 1952 – 1953)</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>8.	Nasionalisme dan Internasionalisme, tahun 1953 (pidato yang diucapkan pada Asian Socialist Conference di Rangoon, tahun 1953)</Text>, nomor:16},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>9.	Karangan–karangan dalam "Sikap", "Suara Sosialis" dan majalah–majalah lain</Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>10. Sosialisme Indonesia Pembangunan, tahun 1983 (kumpulan tulisan Sutan Sjahrir diterbitkan oleh Leppenas)</Text>, nomor:18},
                {teks: <Text style={{fmarginTop:5,fontSize:25,textAlign:'justify'}}>Jabatan</Text>, nomor:19},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>1. Perdana Menteri pertama Republik Indonesia</Text>, nomor:20},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>2. Ketua Partai Sosialis Indonesia (PSI)</Text>, nomor:21},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>3. Ketua delegasi Republik Indonesia pada Perundingan Linggarjati</Text>, nomor:22},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>4. Duta Besar Keliling (Ambassador-at-Large) Republik Indonesia</Text>, nomor:23},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:24},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:25},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:26},
                
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>SULTAN SJAHRIR</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default sultan;