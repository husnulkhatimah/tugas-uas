import React, { Component } from 'react';
import {StyleSheet, Text, View, TouchableOpacity, ScrollView,Image,FlatList, Dimensions, ImageBackground } from 'react-native';

const numColumns = 2;
const WIDTH = Dimensions.get('window').width;
class welcome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Sejarah Indonesia')}><Image source={require('../gambar/indonesia.jpg')} style={styles.ukuran}/></TouchableOpacity>, nomor:1},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Teks Proklamasi')}><Image source={require('../gambar/proklamasi.jpg')} style={styles.ukuran}/></TouchableOpacity>, nomor:2},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Teks Pancasila')}><Image source={require('../gambar/pancasila.jpg')} style={styles.ukuran}/></TouchableOpacity>, nomor:3},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Teks Sumpah Pemuda')}><Image source={require('../gambar/sumpah.jpg')} style={styles.ukuran}/></TouchableOpacity>, nomor:4},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Teks UUD 45')}><Image source={require('../gambar/uud.jpg')} style={styles.ukuran}/></TouchableOpacity>, nomor:5},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Provinsi Indonesia')}><Image source={require('../gambar/provinsi.jpg')} style={styles.ukuran}/></TouchableOpacity>, nomor:6},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Pahlawan Indonesia')}><Image source={require('../gambar/pahlawan.jpg')} style={styles.ukuran}/></TouchableOpacity>, nomor:7},
                {gambar: <TouchableOpacity onPress={() => this.props.navigation.navigate('Sejarah Pahlawan Indonesia')}><Image source={require('../gambar/sejarah.jpg')} style={styles.ukuran}/></TouchableOpacity>, nomor:8},
            ],
        };
    }
    render() {
        return(
            <View style={{flex:1}}>
                <View style={{alignItems:'center',justifyContent:'center',flex:1}}>
                    <ImageBackground source={require('../gambar/bendera.jpg')} style={{width:500,height:100}}>
                        <Text style={{fontSize:60,marginTop:25,textAlign:'center'}}>Indonesia Ku</Text>                
                    </ImageBackground>
                    </View>
                <View style={{flex:8}}>
                    <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                       <View style={styles.gambar}>
                            {item.gambar}
                        </View>
                        )}
                        keyExtractor={(item) => item.nomor}
                        numColumns={numColumns}
                    />
                </View>
            </View>
        );
    }
}

const styles=StyleSheet.create({
    gambar : {
       height: WIDTH / (numColumns * 1),
       margin:5,
       padding:5,
       backgroundColor:'white',
       alignItems:'center',
       justifyContent:'center',
    },
    ukuran : {
        width: 190,
        height: 200,
    }
});
export default welcome;