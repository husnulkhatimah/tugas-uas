import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class hasanuddin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Sultan Hasanuddin  lahir di Gowa, Sulawesi Selatan, 12 Januari 1631. meninggal di Gowa, Sulawesi Selatan, 12 Juni 1670 pada umur 39 tahun, adalah Sultan Gowa ke-16 dan pahlawan nasional Indonesia yang terlahir dengan nama Muhammad Bakir I Mallombasi Daeng Mattawang Karaeng Bonto Mangape. </Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Sultan Hasanuddin, merupakan putera dari Raja Gowa ke-15, I Manuntungi Daeng Mattola Karaeng Lakiyung Sultan Muhammad Said. Sultan Hasanuddin memerintah Kerajaan Gowa mulai tahun 1653 sampai 1669. Kesultanan Gowa adalah merupakan kesultanan besar di Wilayah Timur Indonesia yang menguasai jalur perdagangan.</Text>, nomor:2},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Biografi</Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Sultan Hasanuddin lahir di Makassar pada 12 Januari 1631. Dia lahir dari pasangan Sultan Malikussaid, Sultan Gowa ke-15, dengan I Sabbe To’mo Lakuntu. Jiwa kepemimpinannya sudah menonjol sejak kecil. Selain dikenal sebagai sosok yang cerdas, dia juga pandai berdagang. Karena itulah dia memiliki jaringan dagang yang bagus hingga Makassar, bahkan dengan orang asing.</Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Hasanuddin kecil mendapat pendidikan keagamaan di Masjid Bontoala. Sejak kecil ia sering diajak ayahnya untuk menghadiri pertemuan penting, dengan harapan dia bisa menyerap ilmu diplomasi dan strategi perang. Beberapa kali dia dipercaya menjadi delegasi untuk mengirimkan pesan ke berbagai kerjaan.</Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Saat memasuki usia 21 tahun, Hasanuddin diamanatkan jabatan urusan pertahanan Gowa. Ada dua versi sejarah yang menjelaskan kapan dia diangkat menjadi raja, yaitu saat berusia 24 tahun atau pada 1655 atau saat dia berusia 22 tahun atau pada 1653. Terlepas dari perbedaan tahun, Sultan Malikussaid telah berwasiat supaya kerajaannya diteruskan oleh Hasanuddin.</Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Selain dari ayahnya, dia memperoleh bimbingan mengenai pemerintahan melalui Mangkubumi Kesultanan Gowa, Karaeng Pattingaloang. Sultan Hasanuddin merupakan guru dari Arung Palakka, salah satu Sultan Bone yang kelak akan berkongsi dengan Belanda untuk menjatuhkan Kesultanan Gowa</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada pertengahan abad ke-17, Kompeni Belanda (VOC) berusaha memonopoli perdagangan rempah-rempah di Maluku setelah berhasil mengadakan perhitungan dengan orang-orang Spanyol dan Portugis. Kompeni Belanda memaksa orang-orang negeri menjual dengan harga yang ditetapkan oleh mereka, selain itu Kompeni menyuruh tebang pohon pala dan cengkih di beberapa tempat, supaya rempah-rempah jangan terlalu banyak. Maka Sultan Hasanuddin menolak keras kehendak itu, sebab yang demikian adalah bertentangan dengan kehendak Allah katanya. Untuk itu Sultan Hasanuddin pernah mengucapkan kepada Kompeni "marilah berniaga bersama-sama, mengadu untuk dengan serba kegiatan". Tetapi Kompeni tidak mau, sebab dia telah melihat besarnya keuntungan di negeri ini, sedang Sultan Hasanuddin memandang bahwa cara yang demikian itu adalah kezaliman.</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada tahun 1660, VOC Belanda menyerang Makassar, tetapi belum berhasil menundukkan Kesultanan Gowa. Tahun 1667, VOC Belanda di bawah pimpinan Cornelis Speelman beserta sekutunya kembali menyerang Makassar. Pertempuran berlangsung di mana-mana, hingga pada akhirnya Kesultanan Gowa terdesak dan semakin lemah, sehingga dengan sangat terpaksa Sultan Hasanuddin menandatangani Perjanjian Bungaya pada tanggal 18 November 1667 di Bungaya. Gowa yang merasa dirugikan, mengadakan perlawanan lagi. Pertempuran kembali pecah pada Tahun 1669. Kompeni berhasil menguasai benteng terkuat Gowa yaitu Benteng Sombaopu pada tanggal 24 Juni 1669. Sultan Hasanuddin wafat pada tanggal 12 Juni 1670.</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Namanya kini diabadikan untuk Universitas Hasanuddin, Kodam XIV/Hasanuddin dan Bandar Udara Internasional Sultan Hasanuddin di Makassar</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:13},
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>SULTAN HASANUDDIN</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default hasanuddin;