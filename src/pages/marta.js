import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class marta extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Martha Christina Tiahahu (lahir di Nusa Laut, Maluku, 4 Januari 1800 – meninggal di Laut Banda, Maluku, 2 Januari 1818 pada umur 17 tahun) adalah seorang gadis dari Desa Abubu di Pulau Nusalaut. Lahir sekitar tahun 1800 dan pada waktu mengangkat senjata melawan penjajah Belanda berumur 17 tahun. Ayahnya adalah Kapitan Paulus Tiahahu, seorang kapitan dari negeri Abubu yang juga pembantu Thomas Matulessy dalam Perang Pattimura tahun 1817 melawan Belanda. </Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Di dalam pertempuran yang sengit di Desa Ouw – Ullath jasirah tenggara Pulau Saparua yang tampak betapa hebat srikandi ini menggempur musuh bersama para pejuang rakyat. Namun akhirnya karena tidak seimbang dalam persenjataan, tipu daya musuh dan pengkhianatan, para tokoh pejuang dapat ditangkap dan menjalani hukuman. Ada yang harus mati digantung dan ada yang dibuang ke Pulau Jawa. Kapitan Paulus Tiahahu divonis hukum mati tembak. Martha Christina Tiahahu berjuang untuk melepaskan ayahnya dari hukuman mati, tetapi ia tidak berdaya dan meneruskan bergerilyanya di hutan, tetapi akhirnya tertangkap dan diasingkan ke Pulau Jawa.</Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Di Kapal Perang Eversten, Martha Christina Tiahahu menemui ajalnya dan dengan penghormatan militer jasadnya diluncurkan di Laut Banda menjelang tanggal 2 Januari 1818. Untuk menghargai jasa dan pengorbanannya, Martha Christina Tiahahu dikukuhkan sebagai Pahlawan Kemerdekaan Nasional oleh Pemerintah Republik Indonesia.</Text>, nomor:3},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Perjuangan </Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada waktu itu, sebagian pasukan rakyat bersama para raja dan patih bergerak ke Saparua untuk membantu perjuangan Kapitan Pattimura sehingga tindakan Belanda yang akan mengambil alih Benteng Beverwijk luput dari perhatian. Guru Soselissa yang memihak Belanda melakukan kontak dengan musuh mengatas-namakan rakyat menyatakan menyerah kepada Belanda. Tanggal 10 Oktober 1817 Benteng Beverwijk jatuh ke tangan Belanda tanpa perlawanan. Sementara itu, di Saparua pertempuran demi pertempuran terus berkobar. Karena semakin berkurangnya persediaan peluru dan mesiu pasukan rakyat mundur ke pegunungan Ulath-Ouw. Di antara pasukan itu terdapat pula Martha Christina Tiahahu beserta para raja dan patih dari Nusalaut.</Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Tanggal 11 Oktober 1817 pasukan Belanda di bawah pimpinan Richemont bergerak ke Ulath, tetapi berhasil dipukul mundur oleh pasukan rakyat. Dengan kekuatan 100 orang prajurit, Meyer beserta Richemont kembali ke Ulath. Pertempuran berkobar kembali, korban berjatuhan di kedua belah pihak.</Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Tanggal 12 Oktober 1817 Vermeulen Kringer memerintahkan serangan umum terhadap pasukan rakyat, ketika pasukan rakyat membalas serangan yang begitu hebat ini dengan lemparan batu, para opsir Belanda menyadari bahwa persediaan peluru pasukan rakyat telah habis. Vermeulen Kringer memberi komando untuk keluar dari kubu-kubu dan kembali melancarkan serangan dengan sangkur terhunus. Pasukan rakyat mundur dan bertahan di hutan, seluruh negeri Ulath dan Ouw diratakan dengan tanah, semua yang ada dibakar dan dirampok habis-habisan.</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Martha Christina dan sang ayah serta beberapa tokoh pejuang lainnya tertangkap dan dibawa ke dalam kapal Eversten. Di dalam kapal ini para tawanan dari Jasirah Tenggara bertemu dengan Kapitan Pattimura dan tawanan lainnya. Mereka diinterogasi oleh Buyskes dan dijatuhi hukuman. Karena masih sangat muda, Buyskes membebaskan Martha Christina Tiahahu dari hukuman, tetapi sang ayah, Kapitan Paulus Tiahahu tetap dijatuhi hukuman mati. Mendengar keputusan tersebut, Martha Christina Tiahahu memandang sekitar pasukan Belanda dengan tatapan sayu namun kuat yang menandakan keharuan mendalam terhadap sang ayah. Tiba-tiba Martha Christina Tiahahu merebahkan diri di depan Buyskes memohonkan ampun bagi sang ayah yang sudah tua, tetapi semua itu sia-sia.</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Tanggal 16 Oktober 1817 Martha Christina Tiahahu beserta sang Ayah dibawa ke Nusalaut dan ditahan di benteng Beverwijk sambil menunggu pelaksanaan eksekusi mati bagi ayahnya. Martha Christina Tiahahu mendampingi sang Ayah pada waktu memasuki tempat eksekusi, kemudian Martha Christina Tiahahu dibawa kembali ke dalam benteng Beverwijk dan tinggal bersama guru Soselissa.</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Dalam suatu Operasi Pembersihan pada bulan Desember 1817 Martha Christina Tiahahu beserta 39 orang lainnya tertangkap dan dibawa dengan kapal Eversten ke Pulau Jawa untuk dipekerjakan secara paksa di perkebunan kopi.</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Selama di atas kapal ini kondisi kesehatan Martha Christina Tiahahu semakin memburuk, ia menolak makan dan pengobatan. Akhirnya pada tanggal 2 Januari 1818, selepas Tanjung Alang, Martha Christina Tiahahu menghembuskan napas yang terakhir. Jenazah Martha Christina Tiahahu disemayamkan dengan penghormatan militer ke Laut Banda.</Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Berdasarkan Surat Keputusan Presiden Republik Indonesia Nomor 012/TK/Tahun 1969, tanggal 20 Mei 1969, Martha Christina Tiahahu secara resmi diakui sebagai pahlawan nasional.</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:15},
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:35,marginTop:7}}>MARTHA CHRISTINA TIAHAHU</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default marta;