import React, { Component } from 'react';
import { View, Text,Image, FlatList  } from 'react-native';

class pancasila extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [
                {gambar: <Image source={require('../gambar/panca.png')}style={{width:410,height:680,marginTop:20}}/>, nomor:1},
                ],
        };
    }
    render() {
        return(
            <View style={{alignItems:'center',backgroundColor:'white',flex:1}}>
                 <View>
                    <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>TEKS PANCASILA</Text>
                </View>
                    <FlatList
                    data = {this.state.data}
                    renderItem = {({item,index}) => (
                        <View >
                            {item.gambar}
                        </View>
                    )}
                    keyExtractor = {(item) => item.nomor}
                    />

        </View>
        )
    }
}
export default pancasila;