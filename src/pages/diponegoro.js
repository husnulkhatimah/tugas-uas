import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class diponegoro extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Diponegoro lahir di Yogyakarta pada tanggal 11 November 1785 dari ibu yang merupakan seorang selir (garwa ampeyan), bernama R.A. Mangkarawati, dari Pacitan dan ayahnya bernama Gusti Raden Mas Suraja, yang di kemudian hari naik tahta bergelar Hamengkubuwana III Pangeran Diponegoro sewaktu dilahirkan bernama Bendara Raden Mas Mustahar, kemudian diubah menjadi Bendara Raden Mas Antawirya. Nama Islamnya adalah 'Abdul Hamid. Setelah ayahnya naik tahta, Bendara Raden Mas Antawirya diwisuda sebagai pangeran dengan nama Bendara Pangeran Harya Dipanegara.</Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Ketika dewasa, Pangeran Diponegoro menolak keinginan sang ayah untuk menjadi raja. Ia sendiri beralasan bahwa posisi ibunya yang bukan sebagai istri permaisuri, membuat dirinya merasa tidak layak untuk menduduki jabatan tersebut. </Text>, nomor:2},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Perang Diponegoro (1825–1830)  </Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Perang Diponegoro atau Perang Jawa diawali dari keputusan dan tindakan Hindia Belanda yang memasang patok-patok di atas lahan milik Diponegoro di Desa Tegalrejo. Tindakan tersebut ditambah beberapa kelakuan Hindia Belanda yang tidak menghargai adat istiadat setempat dan eksploitasi berlebihan terhadap rakyat dengan pajak tinggi, membuat Pangeran Diponegoro semakin muak hingga mencetuskan sikap perlawanan sang Pangeran. </Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Di beberapa literatur yang ditulis oleh Hindia Belanda, menurut mantan Menteri Pendidikan dan Kebudayaan Professor Wardiman Djojonegoro, terdapat pembelokan sejarah penyebab perlawanan Pangeran Diponegoro karena sakit hati terhadap pemerintahan Hindia Belanda dan keraton, yang menolaknya menjadi raja. Padahal, perlawanan yang dilakukan disebabkan sang Pangeran ingin melepaskan penderitaan rakyat miskin dari sistem pajak Hindia Belanda dan membebaskan Istana dari madat. </Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Keputusan dan sikap Pangeran Diponegoro yang menentang Hindia Belanda secara terbuka kemudian mendapat dukungan dan simpati dari rakyat. Atas saran dari sang paman, yakni GPH Mangkubumi, Pangeran Diponegoro menyingkir dari Tegalrejo dan membuat markas di Gua Selarong. Saat itu, Diponegoro menyatakan bahwa perlawanannya adalah perang sabil, perlawanan menghadapi kaum kafir. Semangat "perang sabil" yang dikobarkan Diponegoro membawa pengaruh luas hingga ke wilayah Pacitan dan Kedu</Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Medan pertempuran Perang Diponegoro mencakup Yogyakarta, Kedu, Bagelen, Surakarta, dan beberapa daerah seperti Banyumas, Wonosobo, Banjarnegara, Weleri, Pekalongan, Tegal, Semarang, Demak, Kudus, Purwodadi, Parakan, Magelang, Madiun, Pacitan, Kediri, Bojonegoro, Tuban, dan Surabaya.</Text>, nomor:7},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Para Panglima Diponegoro </Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>a. Kiai Madja</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>b. Sentot Prawirodirdjo</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>c. Kerta Pengalasan</Text>, nomor:11},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Para Pendamping </Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>a. Joyosuroto</Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>b. Banthengwareng</Text>, nomor:14},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Akhir Hayat Diponegoro</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Ketika ditangkap dan akan diasingkan ke Manado dengan menggunakan Kapal Pollux, kondisi Pangeran Diponegoro sudah dalam keadaan lemah, muntah-muntah akibat mabuk laut, dan terkena sakit Malaria. Di atas kapal, Letnan Knooerle, yang merupakan ajudan dari Gubernur Jenderal van den Bosch (arsitek Tanam Paksa), mengawal pengasingan Diponegoro. Sering kali mereka berdua terlibat dalam percakapan dan salah satu percakapannya adalah ketika Diponegoro mempertanyakan kepada Knoorle, apakah sudah menjadi kebiasaan bangsa Eropa untuk mengasingkan pemimpin yang kalah perang ke sebuah pulau terpencil yang jauh dari sanak saudaranya. Mendapat pertanyaan itu, Knoorle menjawab bahwa Pangeran Diponegoro diperlakukan sama dengan Napoleon Bonaparte, yang sama-sama diasingkan dalam usia 40 tahunan. Knoorle mengatakan pemerintahan Hindia Belanda tidak ingin peristiwa Napoleon yang ditangkap dan diasingkan ke Pulau Elba berhasil kabur dan memimpin perang lagi lalu berhasil dikalahkan sehingga dibuang ke pulau yang lebih terasing lagi, yakni St Helena hingga wafat. </Text>, nomor:16},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pangeran Diponegoro dan rombongannya, yakni istri, dua anaknya, dan 23 pengikutnya tiba di Manado pada 12 Juni 1830. Awalnya, Diponegoro akan ditempatkan di Tondano, namun Knoorle diberitahu oleh Pietermaat, seorang residen setempat bahwa Kiai Madja beserta 62 pengikutnya baru saja tiba di Tondano dari Ambon, sehingga akhirnya Knoorle memutuskan Diponegoro ditahan di Benteng Manado untuk sementara waktu agar tidak ketemu dengan Kiai Madja. Diponegoro berada di Benteng Manado atau Fort Nieuw Amsterdam sejak Juni 1830 hingga Juni 1833. Selanjutnya, pada tahun 1833, Diponegoro dipindahkan ke Makassar secara diam-diam dan ditempatkan di Benteng Fort Rotterdam selama sebelas tahun. Diponegoro menolak upaya Hindia Belanda untuk memindahkannya ke tempat pengasingan baru dan ingin menghabiskan akhir hayatnya di Makassar. </Text>, nomor:17},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Peninggalan Bersejarah </Text>, nomor:18},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>1. Babad Dipanagara </Text>, nomor:19},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>2. Keris</Text>, nomor:20},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>3. Tongkat</Text>, nomor:21},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>4. Tombak</Text>, nomor:22},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>5. Benda lainnya </Text>, nomor:23},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>Menurut sejarawan Peter Carey, selain keris dan tongkat, saat ini masih ada dua peninggalan Pangeran Diponegoro, yakni surat asli sang Pangeran kepada ibunda dan anak sulungnya serta tali kuda, yang masih tersimpan di Belanda. </Text>, nomor:24},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>Sementara itu, menurut Direktur Museum Sejarah Kolonial Bronbeek di Arnhem, Pauljac Verhoeven, benda peninggalan Pangeran Diponegoro yakni tali kekang dan pelana yang telah memiliki nomor arsip telah dikembalikan kepada pemerintah Indonesia. </Text>, nomor:25},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Penghargaan sebagai Pahlawan</Text>, nomor:26},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Atas penghormatan terhadap jasa-jasa Diponegoro melawan penjajahan Hindia Belanda, kota-kota besar di Indonesia banyak yang memiliki nama Jalan Pangeran Diponegoro, seperti di Kota Semarang terdapat nama Jalan Pangeran Diponegoro, Stadion Diponegoro, Universitas Diponegoro (Undip), dan Kodam IV/Diponegoro. Selain itu, ada beberapa patung yang dibuat sebagai penghargaan, seperti Patung Diponegoro di Undip Pleburan, Patung Diponegoro di Kodam IV/Diponegoro dan di pintu masuk Undip Tembalang.</Text>, nomor:27},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Penghargaan tertinggi justru diberikan oleh Dunia, pada 21 Juni 2013, UNESCO menetapkan Babad Diponegoro sebagai Warisan Ingatan Dunia (Memory of the World). Babad Diponegoro merupakan naskah klasik yang dibuat sendiri oleh Pangeran Diponegoro ketika diasingkan di Manado, Sulawesi Utara, pada 1832-1833. Babad ini bercerita mengenai kisah hidup Pangeran Diponegoro yang memiliki nama asli Raden Mas Antawirya.</Text>, nomor:28},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:29},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:30},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:31},

                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>PANGERAN DIPONEGORO</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default diponegoro;