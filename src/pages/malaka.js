import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class malaka extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Nama asli Tan Malaka adalah Sutan Ibrahim, sedangkan Tan Malaka adalah nama semi-bangsawan yang ia dapatkan dari garis turunan ibu. Nama lengkapnya adalah Sutan Ibrahim Gelar Datuk Sutan Malaka. Tanggal kelahirannya masih diperdebatkan, sedangkan tempat kelahirannya sekarang dikenal dengan nama Nagari Pandam Gadang, Suliki (kini masuk Gunuang Omeh), Lima Puluh Kota, Sumatra Barat. Ayah dan Ibunya bernama HM. Rasad, seorang karyawan pertanian, dan Rangkayo Sinah, putri orang yang disegani di desa. </Text>, nomor:1},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Biografi</Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Nama asli Tan MalakMadiloga adalah Sutan Ibrahim, sedangkan Tan Malaka adalah nama semi-bangsawan yang ia dapatkan dari garis turunan ibu. Nama lengkapnya adalah Sutan Ibrahim Gelar Datuk Sutan Malaka. Tanggal kelahirannya masih diperdebatkan, sedangkan tempat kelahirannya sekarang dikenal dengan nama Nagari Pandam Gadang, Suliki (kini masuk Gunuang Omeh), Lima Puluh Kota, Sumatra Barat. Ayah dan Ibunya bernama HM. Rasad, seorang karyawan pertanian, dan Rangkayo Sinah, putri orang yang disegani di desa. Semasa kecilnya, Tan Malaka senang mempelajari ilmu agama dan berlatih pencak silat. Pada tahun 1908, ia didaftarkan ke Kweekschool (sekolah guru negara) di Fort de Kock. Menurut GH Horensma, salah satu guru di sekolahnya itu, Tan Malaka adalah murid yang cerdas, meskipun kadang-kadang tidak patuh. Di sekolah ini, ia menikmati pelajaran bahasa Belanda, sehingga Horensma menyarankan agar ia menjadi seorang guru di sekolah Belanda.  Ia juga adalah seorang pemain sepak bola yang bertalenta. Setelah lulus dari sekolah itu pada tahun 1913, ia ditawari gelar datuk dan seorang gadis untuk menjadi tunangannya. Namun, ia hanya menerima gelar datuk.  Gelar tersebut diterimanya dalam sebuah upacara tradisional pada tahun 1913. </Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}> Pada 21 Februari 1949, Tan Malaka dieksekusi mati oleh Suradi Tekebek, orang yang diberi tugas oleh Letnan Dua Soekotjo dari Batalion Sikatan, Divisi Brawijaya. Setelah dieksekusi ia dimakamkan di tengah hutan dekat markas Soekotjo. Kematiannya tanpa dibuat laporan maupun pemeriksaan lebih lanjut.  </Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Madilog dan Gerpolek, keduanya acapkali dianggap merupakan karya penting dari Tan Malaka. Madilog merupakan istilah baru dalam cara berpikir, dengan menghubungkan ilmu bukti serta mengembangkan dengan jalan dan metode yang sesuai dengan akar dan urat kebudayaan Indonesia sebagai bagian dari kebudayaan dunia. Bukti adalah fakta dan fakta adalah lantainya ilmu bukti. Bagi filsafat, idealisme yang pokok dan pertama adalah budi (mind), kesatuan, pikiran dan penginderaan. Filsafat materialisme menganggap alam, benda dan realita nyata obyektif sekeliling sebagai yang ada, yang pokok dan yang pertama.</Text>, nomor:5},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Madilog dan Gerpolek  </Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Madilog dan Gerpolek, keduanya acapkali dianggap merupakan karya penting dari Tan Malaka. Madilog merupakan istilah baru dalam cara berpikir, dengan menghubungkan ilmu bukti serta mengembangkan dengan jalan dan metode yang sesuai dengan akar dan urat kebudayaan Indonesia sebagai bagian dari kebudayaan dunia. Bukti adalah fakta dan fakta adalah lantainya ilmu bukti. Bagi filsafat, idealisme yang pokok dan pertama adalah budi (mind), kesatuan, pikiran dan penginderaan. Filsafat materialisme menganggap alam, benda dan realita nyata obyektif sekeliling sebagai yang ada, yang pokok dan yang pertama</Text>, nomor:7},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Pahlawan </Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Setelah Indonesia merdeka, Tan Malaka menjadi salah satu pelopor sayap kiri. Ia juga terlibat dalam Peristiwa 3 Juli 1946 dengan membentuk Persatuan Perjuangan dan disebut-sebut sebagai otak dari penculikan Sutan Syahrir yang pada waktu itu merupakan perdana menteri. Karena itu ia dijebloskan ke dalam penjara tanpa pernah diadili selama dua setengah tahun. Setelah meletus pemberontakan FDR/PKI di Madiun, September 1948 dengan pimpinan Musso dan Amir Syarifuddin, Tan Malaka dikeluarkan begitu saja dari penjara.</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Setelah pemberontakan PKI/FDR di Madiun ditumpas pada akhir November 1948, Tan Malaka menuju Kediri dan mengumpulkan sisa-sisa pemberontak PKI/FDR yang saat itu ada di Kediri, dari situ ia membentuk pasukan Gerilya Pembela Proklamasi. Pada bulan Februari 1949, Tan Malaka ditangkap bersama beberapa orang pengikutnya di Pethok, Kediri, Jawa Timur dan mereka ditembak mati di sana. Tidak ada satupun pihak yang tahu pasti dimana makam Tan Malaka dan siapa yang menangkap dan menembak mati dirinya dan pengikutnya. Tapi akhirnya misteri tersebut terungkap dari penuturan Harry A. Poeze, seorang Sejarawan Belanda yang menyebutkan bahwa yang menangkap dan menembak mati Tan Malaka pada tanggal 21 Februari 1949 adalah pasukan TNI dibawah pimpinan Letnan II Soekotjo (pernah jadi Wali Kota Surabaya). Batalyon tersebut di bawah komando Brigade S yang panglimanya adalah Letkol Soerachmad. dari Batalyon Sikatan, Divisi Brawijaya. </Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Keputusan Presiden RI No. 53, yang ditandatangani Presiden Soekarno 28 Maret 1963 menetapkan Tan Malaka sebagai Pahlawan Nasional.</Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Salah satu roman Patjar Merah yang terkenal adalah roman karangan Matu Mona yang berjudul Spionnage-Dienst. Nama patjar merah sendiri berasal dari karya Baronesse Orczy yang berjudul Scarlet Pimpernel, yang berkisah tentang seorang pahlawan Revolusi Prancis.</Text>, nomor:12},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Tan Malaka dalam fiksi </Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Dengan julukan Patjar Merah Indonesia, Tan Malaka merupakan tokoh utama beberapa roman picisan yang terbit di Medan. Roman-roman tersebut mengisahkan petualangan Patjar Merah, seorang aktivis politik yang memperjuangkan kemerdekaan Indonesia, dari kolonialisme Belanda. Karena kegiatannya itu, ia harus melarikan diri dari Indonesia dan menjadi buruan polisi rahasia internasional.</Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Salah satu roman Patjar Merah yang terkenal adalah roman karangan Matu Mona yang berjudul Spionnage-Dienst. Nama patjar merah sendiri berasal dari karya Baronesse Orczy yang berjudul Scarlet Pimpernel, yang berkisah tentang seorang pahlawan Revolusi Prancis.</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Dalam cerita-cerita tersebut selain Tan Malaka muncul juga tokoh-tokoh PKI dan PARI lainnya, yaitu Musso (sebagai Paul Mussotte), Alimin (Ivan Alminsky), Semaun (Semounoff), Darsono (Darsnoff), Djamaluddin Tamin (Djalumin) dan Soebakat (Soe Beng Kiat). Kisah-kisah fiksi ini turut memperkuat legenda Tan Malaka di Indonesia, terutama di Sumatra.</Text>, nomor:16},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Belakangan, selepas reformasi kemudian muncul pula dua novel yang mengisahkan perjalanan hidup Tan Malaka. Tiga buku pertama ditulis oleh Matu Mona, sementara yang keempat dan kelima ditulis oleh Yusdja. Sedangkan novel yang keenam dan ketujuh masih-masing ditulis oleh Peter Dantovski dan Hendri Teja.</Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:18},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:19},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:20},
                
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>TAN MALAKA</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default malaka;