import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class sudirman extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Jenderal Besar TNI (Anumerta) Raden Soedirman lahir di Purbalingga, 24 Januari 1916 meninggal di Magelang, 29 Januari 1950 pada umur 34 tahun.</Text>, nomor:1},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Kehidupan awal </Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Soedirman lahir dari pasangan Karsid Kartawiraji dan Siyem saat pasangan ini tinggal di rumah saudari Siyem yang bernama Tarsem di Rembang, Bodas Karangjati, Purbalingga, Hindia Belanda. Tarsem sendiri bersuamikan seorang camat bernama Raden Cokrosunaryo. Menurut catatan keluarga, Soedirman dinamai oleh pamannya lahir pada Minggu pon di bulan Maulud dalam penanggalan Jawa; pemerintah Indonesia kemudian menetapkan 24 Januari 1916 sebagai hari ulang tahun Soedirman. Karena kondisi keuangan Cokrosunaryo yang lebih baik, ia mengadopsi Soedirman dan memberinya gelar Raden, gelar kebangsawanan pada suku Jawa. Soedirman tidak diberitahu bahwa Cokrosunaryo bukanlah ayah kandungnya sampai ia berusia 18 tahun. Setelah Cokrosunaryo pensiun sebagai camat pada akhir 1916, Soedirman ikut dengan keluarganya ke Manggisan, Cilacap. Di tempat inilah ia tumbuh besar. Di Cilacap, Karsid dan Siyem memiliki seorang putra lain bernama Muhammad Samingan. Karsid meninggal dunia saat Soedirman berusia enam tahun, dan Siyem menitipkan kedua putranya pada saudara iparnya dan kembali ke kampung halamannya di Parakan Onje, Ajibarang.</Text>, nomor:3},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Masa pendudukan Jepang</Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Ketika Perang Dunia II pecah di Eropa, diperkirakan bahwa Jepang, yang telah bergerak mendekati China daratan, akan berupaya menginvasi Hindia. Sebagai tanggapan, pemerintah kolonial Belanda yang sebelumnya membatasi pelatihan militer bagi pribumi mulai mengajari rakyat cara-cara menghadapi serangan udara. Menindaklanjuti hal ini, Belanda kemudian membentuk tim Persiapan Serangan Udara. Soedirman, yang disegani oleh masyarakat, diminta untuk memimpin tim di Cilacap. Soedirman mendirikan pos pemantau di seluruh daerah. Ia dan Belanda juga menangani pesawat udara yang menjatuhkan material untuk mensimulasikan pengeboman. </Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Jepang mulai menduduki Hindia pada awal 1942 setelah memenangkan beberapa pertempuran melawan pasukan Belanda dan tentara Koninklijk Nederlands-Indische Leger (KNIL) yang dilatih oleh Belanda. Pada 9 Maret 1942, Gubernur Jenderal Tjarda van Starkenborgh Stachouwer dan Jenderal KNIL Hein ter Poorten menyerah.</Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada awal 1944, setelah menjabat selama satu tahun sebagai perwakilan di dewan karesidenan yang dijalankan oleh Jepang (Syu Sangikai), Soedirman diminta untuk bergabung dengan tentara Pembela Tanah Air (PETA). Jepang sendiri mendirikan PETA pada Oktober 1943 untuk membantu menghalau invasi Sekutu, dan berfokus dalam merekrut para pemuda yang belum "terkontaminasi" oleh pemerintah Belanda. </Text>, nomor:7},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Revolusi Nasional</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>1. Panglima besar</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>2. Negosiasi dengan Belanda</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>3. Perang gerilya</Text>, nomor:11},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Pasca-perang dan kematian</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada awal Agustus, Soedirman mendekati Soekarno dan memintanya untuk melanjutkan perang gerilya. Soedirman tidak percaya bahwa Belanda akan mematuhi Perjanjian Roem-Royen, belajar dari kegagalan perjanjian sebelumnya. Soekarno tidak setuju, yang menjadi pukulan bagi Soedirman. Soedirman menyalahkan ketidak-konsistenan pemerintah sebagai penyebab penyakit tuberkulosisnya dan kematian Oerip pada 1948, ia mengancam akan mengundurkan diri dari jabatannya, namun Soekarno juga mengancam akan melakukan hal yang sama. Setelah ia berpikir bahwa pengunduran dirinya akan menyebabkan ketidakstabilan, Soedirman tetap menjabat, dan gencatan senjata di seluruh Jawa mulai diberlakukan pada tanggal 11 Agustus 1949. </Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Soedirman terus berjuang melawan TBC dengan melakukan pemeriksaan di Panti Rapih. Ia menginap di Panti Rapih menjelang akhir tahun, dan keluar pada bulan Oktober; ia lalu dipindahkan ke sebuah sanatorium di dekat Pakem. Akibat penyakitnya ini, Soedirman jarang tampil di depan publik. Ia dipindahkan ke sebuah rumah di Magelang pada bulan Desember. Pada saat yang bersamaan, pemerintah Indonesia dan Belanda mengadakan konferensi panjang selama beberapa bulan yang berakhir dengan pengakuan Belanda atas kedaulatan Indonesia pada 27 Desember 1949. Meskipun sedang sakit, Soedirman saat itu juga diangkat sebagai panglima besar TNI di negara baru bernama Republik Indonesia Serikat. Pada 28 Desember, Jakarta kembali dijadikan sebagai ibu kota negara. </Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Soedirman wafat di Magelang pada pukul 18.30 malam pada tanggal 29 Januari 1950, Jenazah Soedirman disemayamkan di Masjid Gedhe Kauman pada sore hari, yang dihadiri oleh sejumlah elit militer dan politik Indonesia maupun asing</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:16},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:18},
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>JENDERAL SUDIRMAN</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default sudirman;