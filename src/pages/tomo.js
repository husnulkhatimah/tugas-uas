import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class tomo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Sutomo lahir di Surabaya, Jawa Timur, 3 Oktober 1920, meninggal di Padang Arafah, Arab Saudi, 7 Oktober 1981 pada umur 61 tahun atau lebih dikenal dengan sapaan akrab Bung Tomo adalah pahlawan nasional Indonesia yang terkenal karena peranannya dalam Pertempuran 10 November 1945.</Text>, nomor:1},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Masa muda</Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Sutomo dilahirkan di Kampung Blauran, Surabaya. Ayahnya bernama Kartawan Tjiptowidjojo, priyayi golongan menengah yang pernah bekerja sebagai pegawai pemerintah, staf perusahaan swasta, asisten kantor pajak, hingga pegawai perusahan ekspor-impor Belanda. Kartawan mengaku mempunyai pertalian darah dengan beberapa pengikut dekat Pangeran Diponegoro.</Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Ibu Sutomo bernama Subastita, seorang perempuan berdarah campuran Jawa Tengah, Sunda, dan Madura anak seorang distributor lokal mesin jahit Singer di wilayah Surabaya yang sebelum pindah ke Surabaya pernah jadi polisi kotapraja dan anggota Sarekat Islam.</Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Walaupun dibesarkan dalam keluarga yang sangat menghargai pendidikan, namun pada usia 12 tahun, Sutomo terpaksa meninggalkan bangku MULO akibat dampak Despresi Besar yang melanda dunia. </Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Sutomo lalu bergabung dengan KBI (Kepanduan Bangsa Indonesia). Pada usia 17 tahun, ia menjadi berhasil menjadi orang kedua di Hindia Belanda yang mencapai peringkat Pandu Garuda. Sebelum pendudukan Jepang pada 1942, peringkat ini hanya dicapai oleh tiga orang Indonesia.</Text>, nomor:6},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Pertempuran 10 November 1945</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Sutomo muda lebih banyak berkecimpung dalam bidang kewartawanan. Ia antaranya menjadi jurnalis lepas untuk harian Soeara Oemoem, harian berbahasa Jawa Ekspres, mingguan Pembela Rakyat, dan majalah Poestaka Timoer. Baru setelah ia mulai bergabung dengan sejumlah kelompok politik dan sosial.</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada 1944, ia terpilih menjadi anggota Gerakan Rakyat Baru dan pengurus Pemuda Republik Indonesia di Surabaya.yang disponsori Jepang. Bisa dibilang, inilah titik awal keterlibatannya dalam Pertempuran 10 November. Dengan posisinya itu, ia bisa mendapatkan akses radio yang lantas berperan besar untuk menyiarkan orasi-orasinya yang membakar semangat rakyat untuk berjuang mempertahankan Indonesia. Terlebih, sejak 12 Oktober 1945 Bung Tomo juga memimpin Barisan Pemberontak Rakyat Indonesia (BPRI) di Surabaya.</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Meskipun pada akhirnya pihak Indonesia kalah dalam pertempuran 10 November 1945, namun rakyat Surabaya dianggap berhasil memukul mundur pasukan Inggris untuk sementara waktu dan kejadian ini dicatat sebagai salah satu peristiwa terpenting dalam sejarah kemerdekaan Indonesia.</Text>, nomor:10},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Setelah kemerdekaan</Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Antara 1950-1956, Bung Tomo masuk dalam Kabinet Perdana Menteri Burhanuddin Harahap sebagai Menteri Negara Urusan Bekas Pejuang Bersenjata/Veteran, merangkap Menteri Sosial (Ad Interim). Sejak 1956 Sutomo menjadi anggota anggota Konstituante mewakili Partai Rakyat Indonesia. Ia menjadi wakil rakyat hingga badan tersebut dibubarkan Sukarno lewat Dekrit Presiden 1959.</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Sutomo memprotes keras kebijakan Sukarno tersebut, termasuk membawanya ke pengadilan meski akhirnya kalah. Akibatnya perlahan ia menarik diri dari dunia politik dan pemerintahan.</Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Di awal Orde Baru, Sutomo kembali muncul sebagai tokoh yang mulanya mendukung Suharto. Namun sejak awal 1970-an, ia mulai banyak mengkritik program-program Suharto, termasuk salah satunya proyek pembangunan Taman Mini Indonesia Indah. Akibatnya pada 11 April 1978 ia ditangkap dan dipenjara selama setahun atas tuduhan melakukan aksi subversif.</Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Sekeluar dari penjara Sutomo tampaknya tidak lagi berminat untuk bersikap vokal pada pemerintah dan memilih memanfaatkan waktu bersama keluarga dan mendidik kelima anaknya. Selain itu Sutomo juga menjadi lebih bersungguh-sungguh dalam kehidupan imannya.</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada 7 Oktober 1981, Sutomo meninggal dunia di Padang Arafah saat sedang menunaikan ibadah haji. Berbeda dengan tradisi memakamkan jemaah haji yang meninggal di tanah suci, jenazah Bung Tomo dibawa pulang ke tanah air. Sesuai wasiatnya, Bung Tomo tidak dimakamkan di taman makam pahlawan, melainkan di Tempat Pemakaman Umum Ngagel Surabaya.</Text>, nomor:16},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Gelar Pahlawan Nasional </Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Bung Tomo resmi dikukuhkan menjadi Pahlawan Nasional pada peringatan Hari Pahlawan tahun 2008 di Istana Merdeka. Sang istri, Ny. Sulistina, menerima langsung surat keputusan bernomor 041/TK/Tahun 2008 yang diserahkan presiden. Pengangkatan ini buah dari desakan pelbagai pihak, termasuk GP Ansor dan Fraksi Partai Golkar DPR.</Text>, nomor:18},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Kontroversi</Text>, nomor:19},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada 1950-an di Surabaya, Bung Tomo beraspirasi untuk membantu kehidupan para tukang becak dengan menginisiasi sebuah perkumpulan koperasi. Dengan uang iuran yang ditarik dari para tukang becak, lantas direncanakan pendirian pabrik sabun yang nantinya akan dikelola sepenuhnya oleh dan untuk tukang becak. Akan tetapi ide pendirian pabrik sabun ini tidak mandek di tengah jalan, tanpa pernah ada pertanggungan-jawaban keuangan. </Text>, nomor:20},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Keluarga</Text>, nomor:21},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Bung Tomo menikahi Sulistina, seorang bekas perawat PMI, pada 19 Juni 1947. Pasangan ini dikaruniai empat orang anak, masing-masing bernama Tin "Titing" Sulistami (lahir 29 Juni 1948), Bambang Sulistomo (lahir 22 April 1950), Sri Sulistami (lahir 16 Agustus 1951), dan Ratna Sulistami (12 November 1958).</Text>, nomor:22},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:23},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:24},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:25},
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>BUNG TOMO</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default tomo;