import React, { Component } from 'react';
import { View, Text, Image, FlatList, Dimensions,StyleSheet,TouchableOpacity  } from 'react-native';

const numColumns = 2;
const WIDTH = Dimensions.get('window').width;

class sejarah extends Component {
    constructor(props){
        super(props);
        this.state = {
            yui : [
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Agus Salim')}><Image source={require('../gambar/agus.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:1,nama:'Agus Salim'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Ki Hadjar Dewantara')}><Image source={require('../gambar/dewantara.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:2,nama:'Ki Hadjar Dewantara'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Raden Dewi Sartika')}><Image source={require('../gambar/dewi.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:3,nama:'Raden Dewi Sartika'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Pangeran Diponegoro')}><Image source={require('../gambar/diponegoro.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:4,nama:'Pangeran Diponegoro'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Tjoet Nyak Dien')}><Image source={require('../gambar/diyen.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:5,nama:'Tjoet Nyak Dien'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Sultan Hasanuddin')}><Image source={require('../gambar/hasanuddin.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:6,nama:'Sultan Hasanuddin'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Hasyim Asyari')}><Image source={require('../gambar/hasim.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:7,nama:'Hasyim Asyari'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Mohammad Hatta')}><Image source={require('../gambar/hatta.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:8,nama:'Mohammad Hatta'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('R.A. Kartini')}><Image source={require('../gambar/kartini.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:9,nama:'R.A. Kartini'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Tan Malaka')}><Image source={require('../gambar/malaka.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:10,nama:'Tan Malaka'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Martha Christina Tiahahu')}><Image source={require('../gambar/marta.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:11,nama:'Martha Christina Tiahahu'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Kapitan Pattimura')}><Image source={require('../gambar/patimura.png')} style={styles.hunu}/></TouchableOpacity>,nomor:12,nama:'Kapitan Pattimura'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Jenderal Soedirman')}><Image source={require('../gambar/sudirman.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:13,nama:'Jenderal Soedirman'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Sultan Sjahrir')}><Image source={require('../gambar/sultan.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:14,nama:'Sultan Sjahrir'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Soekarno')}><Image source={require('../gambar/soekarno.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:15,nama:'Soekarno'},
                {cu:<TouchableOpacity onPress={() => this.props.navigation.navigate('Bung Tomo')}><Image source={require('../gambar/tomo.jpg')} style={styles.hunu}/></TouchableOpacity>,nomor:16,nama:'Bung Tomo'},
            ]
        }
    }
    render() {
        return(
            <View>
                <FlatList
                data = {this.state.yui}
                renderItem = {({item,index}) => (
                    <View style={{height:WIDTH/(numColumns*2),padding:5,borderRadius:5,justifyContent:'center',alignItems:'center',marginTop:50,marginBottom:65}}>
                        {item.cu}
                        <Text style={{fontSize:20,marginTop:5}}>{item.nama}</Text>
                    </View>
                )}
                keyExtractor = {(item) => item.nomor}
                numColumns = {numColumns}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
    hunu:{
        width:200,
        height:190,
    }
})
export default sejarah;