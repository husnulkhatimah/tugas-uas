import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class dewi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Dewi Sartika lahir dari keluarga Sunda yang ternama, yaitu R. Rangga Somanegara dan R. A. Rajapermas di Cicalengka pada 4 Desember 1884. Ketika masih kanak-kanak, ia selalu bermain peran menjadi seorang guru ketika seusai sekolah bersama teman-temannya. Setelah ayahnya meninggal, ia tinggal bersama dengan pamannya. Ia menerima pendidikan yang sesuai dengan budaya Sunda oleh pamannya, meskipun sebelumnya ia sudah menerima pengetahuan mengenai budaya barat. Pada tahun 1899, ia pindah ke Bandung. </Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Pada 16 Januari 1904, ia membuat sekolah yang bernama Sekolah Isteri di Pendopo Kabupaten Bandung. Sekolah tersebut kemudian direlokasi ke Jalan Ciguriang dan berubah nama menjadi Sekolah Kaoetamaan Isteri pada tahun 1910. Pada tahun 1912, sudah ada sembilan sekolah yang tersebar di seluruh Jawa Barat, lalu kemudian berkembang menjadi satu sekolah tiap kota maupun kabupaten pada tahun 1920. Pada September 1929, sekolah tersebut berganti nama menjadi Sekolah Raden Dewi </Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Ia meninggal pada 11 September 1947 di Cineam ketika dalam masa perang kemerdekaan. </Text>, nomor:3},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Penghargaan </Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Ia dianugerahi gelar Orde van Oranje-Nassau pada ulang tahun ke-35 Sekolah Kaoetamaan Isteri sebagai penghargaan atas jasanya dalam memperjuangkan pendidikan. Pada 1 Desember 1966, ia diakui sebagai Pahlawan Nasional</Text>, nomor:5},
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>RADEN DEWI SARTIKA</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default dewi;