import React, { Component } from 'react';
import { View, Text, ScrollView,Image,FlatList } from 'react-native';

class provinsi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>SEJARAH PROVINSI INDONESIA</Text>, nomor:1},
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Provinsi di Indonesia dibagi dalam daerah-daerah. Daerah-daerah tersebut diatur dan ditetapkan dengan undang-undang atau peraturan yang setara dengan undang-undang. Ada juga beberapa provinsi atau daerah yang bersifat khusus atau istimewa. Biasanya daerah atau provinsi istimewa itu memiliki peraturan yang berbeda dengan provinsi lainnya. Di antara 34 provinsi tersebut ada 5 provinsi yang memiliki sifat istimewa yaitu, Aceh, Jakarta, Yogyakarta, Papua dan Papua Barat.</Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Provinsi Aceh menggunakan hukum syariah sebagai hukum pidana di provinsi tersebut. Provinsi Jakarta adalah Daerah Khusus Ibukota. Provinsi Yogyakarta adalah Daerah Istimewa yang dikepalai oleh Sultan Hamengkubuwono sebagai gubernurnya selama turun temurun dan Paku Alam sebagai wakilnya yang juga turun temurun. Provinsi Papua dan Papua Barat dibagi lagi secara resmi menjadi tujuh unit geografis</Text>, nomor:3},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Selama perjalanannya setelah kemerdekaan diproklamasikan, Indonesia telah membentuk 34 provinsi. Ada beberapa provinsi yang dimekarkan dan sebagian diubah bentuknya bahkan ada juga yang dibubarkan.</Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>1. Aceh</Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Aceh diresmikan sebagai provinsi pada tanggal 7 Desember 1959. Memiliki luas wilayah 57.956,00 km persegi. Saat ini ada 5.371.532 penduduk yang menempati provinsi Aceh. Ibukota dari provinsi Aceh adalah Banda Aceh. Mayoritas agama dari penduduk Aceh adalah Islam.</Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>2. Bali</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi Bali memiliki Ibukota yang bernama Denpasar. Bali mulai diresmikan sebagai provinsi pada tanggal 14 Agustus 1958. Saat ini populasi di Provinsi Bali ada 4.104.900 jiwa dengan luas daerah 5.780,06 km persegi. Mayoritas penduduk Bali memeluk agama Hindu.</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>3. Bangka Belitung</Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Kepulauan Bangka Belitung diresmikan pada tanggal 21 November tahun 2000. Memiliki luas daerah 16.424,05 km persegi dengan penduduk sebanyak 1.343.900 jiwa. Ibukota dari Bangka Belitung adalah Pangkalpinang. Setengah penduduk Bangka Belitung memeluk agama Islam.</Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>4. Banten</Text>, nomor:11},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Banten adalah provinsi yang memiliki Ibukota Serang. Provinsi Banten memiliki penduduk sebanyak 11.704.877 jiwa dengan mayoritas memeluk agama Islam. Provinsi ini mulai diresmikan oleh Pemerintah pada tanggal 4 Oktober tahun 2000. Provinsi Banteng memiliki luas daerah sebesar 9.662,92 km persegi.</Text>, nomor:12},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>5. Bengkulu</Text>, nomor:13},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi yang berada di Sumatera ini pertama kali diresmikan sebagai provinsi pada tanggal 18 November 1968. Provinsi Bengkulu memiliki Ibukota yang bernama sama, yaitu Bengkulu. Provinsi yang memiliki luas daerah sebesar 19.919,33 km persegi ini diduduki oleh sebanyak 1.884.800 jiwa dengan mayoritas memeluk agama Islam.</Text>, nomor:14},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>6. Jawa Tengah</Text>, nomor:15},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi Jawa Tengah diresmikan oleh pemerintah sebagai provinsi pada tangga; 4 Maret 1950. Jawa Tengah memiliki Semarang sebagai Ibukotanya. Provinsi Jawa Tengah ditempati oleh 3.553.100 penduduk dengan luas daerah sebesar 3.133,15 km persegi.  85% Penduduk dari Provinsi Jawa Tengah memeluk agama Islam.</Text>, nomor:16},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>7. Kalimantan Tengah</Text>, nomor:17},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Palangkaraya adalah Ibukota dari Provinsi Kalimantan tengah. Kalimantan tengah pertama kali diresmikan sebagai provinsi pada tanggal 2 Juli 1958. Provinsi ini memiliki luas daerah sebesar 153.564,50 yang ditempati oleh sebanyak 2.439.858 jiwa. Sebagian besar penduduk Kalimantan Tengah menganut agama Islam.</Text>, nomor:18},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>8. Sulawesi Tengah</Text>, nomor:19},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Sulawesi Tengah pertama kali diresmikan sebagai provinsi pada tanggal 23 September 1964. Ibukota dari Provinsi Sulawesi Tengah adalah Kota Palu. Ditempati oleh 2.831.283 penduduk, mayoritas dari penduduk Sulawesi Tengah menganut agama Islam. Sulawesi tengah adalah salah satu provinsi yang memiliki wilayah paling luas di antara provinsi lainnya di Pulau Sulawesi dengan luas daerah sebesar 61.841,29 km persegi.</Text>, nomor:20},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>9. Jawa timur</Text>, nomor:21},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Ibukota dari Provinsi Jawa Timur adalah Surabaya. Provinsi Jawa Timur pertama kali diresmikan pada tanggal 4 Juli 1950. Penduduk yang ada di Jawa Timur sebanyak 38.610.202 jiwa dan mayoritas menganut agama Islam. Provinsi Jawa Timur memiliki luas daerah sebesar 37.799,75 km persegi.</Text>, nomor:22},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>10. Kalimantan Timur</Text>, nomor:23},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Kalimantan Timur adalah Provinsi Indonesia yang berbatasan dengan Negara Malaysia. Ibukota dari Provinsi Kalimantan Timur adalah Samarinda. Pertama kali diresmikan oleh Pemerintah pada 2 Juli 1958. Kalimantan Timur memiliki luas daerah sebesar 129.066,64 km persegi dengan penduduk sebanyak 3.351.432 yang kebanyakan memeluk agama </Text>, nomor:24},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>11. Nusa Tenggara Timur</Text>, nomor:25},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Nusa Tenggara Timur diduduki oleh penduduk sebanyak 5.325.566 jiwa dengan mayoritas memeluk agama Kristen. Ibu kota dari Provinsi Nusa Tenggara Timur adalah kota Kupang. Nusa Tenggara Timur memiliki 22 kabupaten/kota dengan total luas daerah 48.718,10 km persegi. Provinsi yang berada di kepulauan Nusa Tenggara ini pertama kali diresmikan pada tanggal 14 Agustus 1958.</Text>, nomor:26},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>12. Gorontalo</Text>, nomor:27},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi Gorontalo diduduki oleh 1.115.633 jiwa dan memiliki luas daerah sebesar 11.257,07 km persegi. Gorontalo memiliki Ibu kota dengan nama yang sama yaitu kota Gorontalo. Provinsi ini memiliki julukan serambi madinah dengan penduduk yang mayoritas memeluk agama Islam. Provinsi Gorontalo lahir pada 22 Desember 2000.</Text>, nomor:28},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>13. Daerah Khusus Ibukota Jakarta</Text>, nomor:29},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Daerah Khusus Ibukota Jakarta adalah Ibukota dari negara Indonesia. Kota Jakarta merupakan kota yang satu tingkat dengan provinsi. Jakarta menjadi kota terbesar di Indonesia. Ibukota Jakarta lahir pada tanggal 28 Agustus 1961. provinsi ini memiliki luas wilayah sebesar 664,01 km persegi dengan 10.012.271 jiwa yang menempati Ibu Kota Jakarta.</Text>, nomor:30},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>14. Jambi</Text>, nomor:31},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi Jambi memiliki Ibukota yang bernama sama dengan provinsinya yaitu, Jambi. Pertama kali diresmikan oleh pemerintah pada tanggal 9 Agustus 1957. Provinsi Jambi memiliki luas daerah sebesar 50.058,16 km persegi dengan penduduk sebanyak 3.344.400 jiwa. 65% dari penduduk Provinsi Jambi menganut agama Islam.</Text>, nomor:32},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>15. Lampung</Text>, nomor:33},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Lampung lahir pada tanggal 18 Maret 1964. Provinsi yang terletak di Selatan Pulau Sumatera ini memiliki Ibukota di kota Bandar Lampung. Provinsi Lampung mempunyai dua kota, Kota Bandar Lampung dan Kota Metro. Provinsi Lampung memiliki luas daerah sebesar 34.623,80 km persegi dengan penduduk sebanyak 8.026.191 jiwa. Mayoritas dari penduduk Lampung memeluk agama Islam.</Text>, nomor:34},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>16. Maluku</Text>, nomor:35},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Maluku memiliki kota Ambon sebagai Ibukotanya. Provinsi Maluku berada di antara perbatasan Laut Seram dan Samudra Hindia. Provinsi yang berada di bagian selatan Kepulauan Maluku ini memiliki luas daerah sebesar 46.914,03 km persegi dengan penduduk sebanyak 1.675.409 jiwa yang sebagian besar penduduknya menganut agama Kristen. Provinsi Maluku pertama kali diresmikan oleh pemerintah pada tanggal 17 Juni 1958.</Text>, nomor:36},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>17. Sulawesi Utara</Text>, nomor:37},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi Sulawesi Utara lahir pada tanggal 13 Desember 1960. Provinsi yang terletak di bagian utara pulau Sulawesi ini memiliki kota Manado sebagai Ibukotanya. Provinsi yang berbatasan dengan laut Maluku dan Samudera Pasifik ini memiliki penduduk sebanyak 2.386.604 jiwa yang mayoritas memeluk agama Kristen. Provinsi Sulawesi Utara memiliki luas daerah sebesar 13.851,64 km persegi.</Text>, nomor:38},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>18. Sumatera Utara</Text>, nomor:39},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Sumatera Utara diresmikan oleh pemerintah pertama kali pada tanggal 29 November 1956. Provinsi ini memiliki luas daerah sebesar 72.981,23 km persegi dengan penduduk sebanyak 15.851.851 jiwa yang mayoritas beragama Islam. Sumatera Utara menjadi provinsi dengan penduduk terbanyak di Indonesia yang menduduki peringkat keempat. Ibu kota dari Sumatera Utara adalah kota Medan.</Text>, nomor:40},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>19. Papua</Text>, nomor:41},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi Papua awalnya memiliki nama Irian Jaya. Bagian timur dari Provinsi Papua adalah negara Papua Nugini. Provinsi Papua memiliki penduduk sebanyak 3.091.047 dengan mayoritas memeluk agama kristen. Ibukota dari Provinsi Papua adalah kota Jayapura. Provinsi yang lahir pada tanggal 10 September 1969 ini memiliki luas daerah sebesar 319.036,05 km persegi.</Text>, nomor:42},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>20. Riau</Text>, nomor:43},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi Riau memiliki kota Pekanbaru sebagai Ibukotanya. Provinsi yang lahir pada tanggal 9 Agustus 1957 ini berada di bagian tengah pantai Timur kepulauan Sumatera. Penduduk dari provinsi Riau sebanyak 60% nya menganut agama Islam. Provinsi Riau memiliki luas daerah sebesar 50.058,16 km persegi dengan penduduk sebanyak 3.344.400 jiwa.</Text>, nomor:44},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>21. Kepulauan Riau</Text>, nomor:45},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi Kepulauan Riau adalah provinsi Indonesia yang berbatasan dengan Kamboja Utara dan Vietnam. Provinsi Kepulauan Riau memiliki kota Tanjungpinang sebagai Ibukotanya. Provinsi ini pertama kali diresmikan oleh pemerintah pada tanggal 25 Oktober tahun 2002. Penduduk yang menempati Kepulauan Riau berjumlah 1.917.415 jiwa dengan mayoritas memeluk agama Islam. kepulauan Riau memiliki luas daerah sebesar 8.201,72 km persegi</Text>, nomor:46},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>22. Sulawesi Tenggara</Text>, nomor:47},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi yang berada di bagian tenggara pulau Sulawesi ini pertama kali diresmikan oleh pemerintah sebagai provinsi pada tanggal 23 September tahun 1964. Provinsi Sulawesi Tenggara memiliki Ibukota kendari. Provinsi yang memiliki luas daerah sebesar 39.067,70 km persegi ini diduduki oleh sebanyak 2.448.081 jiwa dengan mayoritas memeluk agama Islam.</Text>, nomor:48},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:10}}>23. Kalimantan Selatan</Text>, nomor:49},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:20}}>Provinsi Kalimantan Selatan lahir pada tanggal 7 Desember 1956. Kalimantan Selatan memiliki populasi penduduk sebanyak 3.922.790 dengan luas daerah sebesar 38.744,23 km persegi. 98% dari penduduk provinsi Kalimantan Selatan memeluk agama Islam. Provinsi Kalimantan Selatan memiliki kota Banjarmasin sebagai ibukotanya.</Text>, nomor:50},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:51},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:52},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:53},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:54},
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>PROVINSI INDONESIA</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default provinsi;