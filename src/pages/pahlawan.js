import React, { Component } from 'react';
import { View, Text,StyleSheet, FlatList, } from 'react-native';

class pahlawan extends Component {
    constructor(props){
        super(props);
        this.state = {
            yui : [
                {iu:<Text style={styles.husnol}>    1.  Agus Salim</Text>,nomor:1},
                {iu:<Text style={styles.husnol}>    2.  Ki Hadjar Dewantara</Text>,nomor:2},
                {iu:<Text style={styles.husnol}>    3.  Raden Dewi Sartika</Text>,nomor:3},
                {iu:<Text style={styles.husnol}>    4.  Pangeran Diponegoro</Text>,nomor:4},
                {iu:<Text style={styles.husnol}>    5.  Tjoet Nyak Dien</Text>,nomor:5},
                {iu:<Text style={styles.husnol}>    6.  Sultan Hasanuddin</Text>,nomor:6},
                {iu:<Text style={styles.husnol}>    7.  Hasyim Asyari</Text>,nomor:7},
                {iu:<Text style={styles.husnol}>    8.  Mohammad Hatta</Text>,nomor:8},
                {iu:<Text style={styles.husnol}>    9.  R.A. Kartini</Text>,nomor:9},
                {iu:<Text style={styles.husnol}>    10.  Tan Malaka</Text>,nomor:10},
                {iu:<Text style={styles.husnol}>    11.  Martha Christina Tiahahu</Text>,nomor:11},
                {iu:<Text style={styles.husnol}>    12.  Kapitan Pattimura</Text>,nomor:12},
                {iu:<Text style={styles.husnol}>    13.  Jenderal Soedirman</Text>,nomor:13},
                {iu:<Text style={styles.husnol}>    14.  Sutan Sjahrir</Text>,nomor:14},
                {iu:<Text style={styles.husnol}>    15.  Soekarno</Text>,nomor:15},
                {iu:<Text style={styles.husnol}>    16.  Bung Tomo</Text>,nomor:16},
            ]
        }
    }
    render() {
        return(
            <View>
               <FlatList
               data = {this.state.yui}
               renderItem = {({item,index}) => (
                   <View>
                       {item.iu}
                   </View>
               )}
               keyExtractor= {(item) => item.nomor}
               />
            </View>
        )
    }
}
const styles= StyleSheet.create({
    husnol:{
        width:425,
        height:50,
        backgroundColor:'whitesmoke',
        color:'black',
        marginTop: 15,
        paddingTop:17,
        fontSize:20,
        fontWeight:'bold'
    }
})
export default pahlawan;