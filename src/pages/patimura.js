import React, { Component } from 'react';
import { View, Text, ScrollView,FlatList } from 'react-native';

class patimura extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data : [ 
                
                {teks: <Text style={{marginTop:9,fontSize:20,textAlign:'justify'}}>     Thomas Matulessy lahir di Haria, Pulau Saparua, Maluku, 8 Juni 1783 – meninggal di Ambon, Maluku, 16 Desember 1817 pada umur 34 tahun, juga dikenal dengan nama Kapitan Pattimura, atau Pattimura adalah pahlawan nasional Indonesia dari Maluku.</Text>, nomor:1},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Menurut buku biografi Pattimura versi pemerintah yang pertama kali terbit, M. Sapija menulis, "Bahwa pahlawan Pattimura tergolong turunan bangsawan dan berasal dari Nusa Ina (Seram)". Ayahnya yang bernama Antoni Matulessy adalah anak dari Kasimiliali Pattimura Mattulessy. Yang terakhir ini adalah putra raja Sahulau. Sahulau merupakan nama orang di negeri yang terletak dalam sebuah teluk di Seram.</Text>, nomor:2},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>Namanya kini diabadikan untuk Universitas Pattimura, Kodam XVI/Pattimura dan Bandar Udara Internasional Pattimura di Ambon.</Text>, nomor:3},
                {teks: <Text style={{marginTop:5,fontSize:25,textAlign:'justify'}}>Perjuangan</Text>, nomor:4},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Sebelum melakukan perlawanan terhadap VOC ia pernah berkarier dalam militer sebagai mantan sersan militer Inggris. </Text>, nomor:5},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pada tahun 1816, pihak Inggris menyerahkan kekuasaannya kepada pihak Belanda dan kemudian Belanda menetapkan kebijakan politik monopoli, pajak atas tanah (landrente), pemindahan penduduk serta pelayaran Hongi (Hongitochten), serta mengabaikan Traktat London I, antara lain dalam pasal 11 memuat ketentuan bahwa Residen Inggris di Ambon harus merundingkan dahulu pemindahan koprs Ambon dengan Gubenur dan dalam perjanjian tersebut juga dicantumkan dengan jelas bahwa jika pemerintahan Inggris berakhir di Maluku maka para serdadu-serdadu Ambon harus dibebaskan dalam artian berhak untuk memilih untuk memasuki dinas militer pemerintah baru atau keluar dari dinas militer, akan tetapi dalam pratiknya pemindahan dinas militer ini dipaksakan. </Text>, nomor:6},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Kedatangan kembali kolonial Belanda pada tahun 1817 mendapat tantangan keras dari rakyat. Hal ini disebabkan karena kondisi politik, ekonomi, dan hubungan kemasyarakatan yang buruk selama dua abad. Rakyat Maluku akhirnya bangkit mengangkat senjata di bawah pimpinan Kapitan Pattimura. Maka pada waktu pecah perang melawan penjajah Belanda tahun 1817, Raja-raja Patih, Para Kapitan, tua-tua adat dan rakyat mengangkatnya sebagai pemimpin dan panglima perang karena berpengalaman dan memiliki sifat-sfat kesatria (kabaressi). Sebagai panglima perang, Kapitan Pattimura mengatur strategi perang bersama pembantunya. Sebagai pemimpin dia berhasil mengkoordinir raja-raja patih dalam melaksanakan kegiatan pemerintahan, memimpin rakyat, mengatur pendidikan, menyediakan pangan dan membangun benteng-benteng pertahanan. Kewibawaannya dalam kepemimpinan diakui luas oleh para raja patih maupun rakyat biasa. Dalam perjuangan menentang Belanda ia juga menggalang persatuan dengan kerajaan Ternate dan Tidore, raja-raja di Bali, Sulawesi dan Jawa. Perang Pattimura yang berskala nasional itu dihadapi Belanda dengan kekuatan militer yang besar dan kuat dengan mengirimkan sendiri Laksamana Buykes, salah seorang Komisaris Jenderal untuk menghadapi Patimura.</Text>, nomor:7},
                {teks: <Text style={{fontSize:20,textAlign:'justify',marginLeft:5}}>Pertempuran-pertempuran yang hebat melawan angkatan perang Belanda di darat dan di laut dikoordinasi Kapitan Pattimura yang dibantu oleh para penglimanya antara lain Melchior Kesaulya, Anthoni Rebook, Philip Latumahina dan Ulupaha. Pertempuran yang menghancurkan pasukan Belanda tercatat seperti perebutan benteng Belanda Duurstede di Saparua, pertempuran di pantai Waisisil dan jasirah Hatawano, Ouw- Ullath, Jazirah Hitu di Pulau Ambon dan Seram Selatan. Perang Pattimura hanya dapat dihentikan dengan politik adu domba, tipu muslihat dan bumi hangus oleh Belanda. Para tokoh pejuang akhirnya dapat ditangkap dan mengakhiri pengabdiannya di tiang gantungan pada tanggal 16 Desember 1817 di kota Ambon. Untuk jasa dan pengorbanannya itu, Kapitan Pattimura dikukuhkan sebagai pahlawan perjuangan kemerdekaan oleh pemerintah Republik Indonesia.</Text>, nomor:8},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:9},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:10},
                {teks: <Text style={{fontSize:20,textAlign:'justify'}}>     </Text>, nomor:11},
                
                ]
        };
    }
    render() {
        return(
            <View style={{alignItems:'center'}}>
                
                <Text style={{fontWeight:'bold',fontSize:40,marginTop:7}}>KAPITAN PATIMURA</Text>
                <FlatList
                        data={this.state.data}
                        renderItem={({item,index}) => (
                        <View>{item.teks}</View>
                        )}
                        keyExtractor={(item) => item.nomor}
                />

            </View>
        )
    }
}
export default patimura;