import firebase from 'firebase';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAJcHZvcpt6fOF1ii0mj4oBSTjaLTZoYTc",
  authDomain: "indonesiaa-790a4.firebaseapp.com",
  databaseURL: "https://indonesiaa-790a4-default-rtdb.firebaseio.com",
  projectId: "indonesiaa-790a4",
  storageBucket: "indonesiaa-790a4.appspot.com",
  messagingSenderId: "263371543533",
  appId: "1:263371543533:web:5878a90582296e64934de4",
  measurementId: "G-JZ1KRK9GPD"
};

firebase.initializeApp(firebaseConfig);

  const FIREBASE = firebase;

  export default FIREBASE;