import React, {useEffect} from 'react' ;
import {Image, StyleSheet, Text, View} from 'react-native';

const splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Welcome');
        }, 4000);
    });
    return (
        <View style={styles.wrapper}>
            <Text style={styles.welcomeText}>Indonesia Ku</Text>
            <Image source={require('../gambar/splash.png')} style={styles.logo} />
        </View>
    );
};

export default splash;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: 'ghostwhite',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo : {
        margin : 10,
        width : 400,
        height : 400,
    },
    welcomeText: {
        fontSize: 50,
        fontWeight: 'bold',
        color: 'black',
        paddingBottom: 20,
    },
});