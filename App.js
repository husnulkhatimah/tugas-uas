import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Splash from './src/pages/splash';
import WelcomeAuth from './src/pages/welcome';
import Indonesia from './src/pages/Sejarah_Indonesia';
import Proklamasi from './src/pages/proklamasi';
import Pancasila from './src/pages/pancasila';
import Sumpah from './src/pages/sumpah';
import Uud from './src/pages/uud';
import Provinsi from './src/pages/provinsi';
import Pahlawan from './src/pages/pahlawan';
import sejarah from './src/pages/sejarah';
import Agus from './src/pages/agus';
import Dewantara from './src/pages/dewantara';
import Dewi from './src/pages/dewi';
import Diponegoro from './src/pages/diponegoro'
import Diyen from './src/pages/diyen';
import Hasanuddin from './src/pages/hasanuddin';
import Hasim from './src/pages/hasim';
import Hatta from './src/pages/hatta';
import Kartini from './src/pages/kartini';
import Malaka from './src/pages/malaka';
import Marta from './src/pages/marta';
import Patimura from './src/pages/patimura';
import Sudirman from './src/pages/sudirman';
import Sultan from './src/pages/sultan';
import Soekarno from './src/pages/soekarno';
import Tomo from './src/pages/tomo';
import Login from './src/pages/Login';

const Stack = createStackNavigator();


const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
            <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Welcome"
            component={WelcomeAuth}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Sejarah Indonesia"
            component={Indonesia}
            />
            <Stack.Screen
            name="Teks Proklamasi"
            component={Proklamasi}
            />
            <Stack.Screen
            name="Teks Pancasila"
            component={Pancasila}
            />
            <Stack.Screen
            name="Teks Sumpah Pemuda"
            component={Sumpah}
            />
            <Stack.Screen
            name="Teks UUD 45"
            component={Uud}
            />
            <Stack.Screen
            name="Provinsi Indonesia"
            component={Provinsi}
            />
            <Stack.Screen
            name="Pahlawan Indonesia"
            component={Pahlawan}
            />
            <Stack.Screen
            name="Sejarah Pahlawan Indonesia"
            component={sejarah}
            />
            <Stack.Screen
            name="Agus Salim"
            component={Agus}
            />
            <Stack.Screen
            name="Ki Hadjar Dewantara"
            component={Dewantara}
            />
            <Stack.Screen
            name="Raden Dewi Sartika"
            component={Dewi}
            />
            <Stack.Screen
            name="Pangeran Diponegoro"
            component={Diponegoro}
            />
            <Stack.Screen
            name="Tjoet Nyak Dien"
            component={Diyen}
            />
            <Stack.Screen
            name="Sultan Hasanuddin"
            component={Hasanuddin}
            />
            <Stack.Screen
            name="Hasyim Asyari"
            component={Hasim}
            />
            <Stack.Screen
            name="Mohammad Hatta"
            component={Hatta}
            />
            <Stack.Screen
            name="R.A. Kartini"
            component={Kartini}
            />
            <Stack.Screen
            name="Tan Malaka"
            component={Malaka}
            />
            <Stack.Screen
            name="Martha Christina Tiahahu"
            component={Marta}
            />
            <Stack.Screen
            name="Kapitan Pattimura"
            component={Patimura}
            />
            <Stack.Screen
            name="Jenderal Soedirman"
            component={Sudirman}
            />
            <Stack.Screen
            name="Sultan Sjahrir"
            component={Sultan}
            />
            <Stack.Screen
            name="Soekarno"
            component={Soekarno}
            />
            <Stack.Screen
            name="Bung Tomo"
            component={Tomo}
            />
            
        </Stack.Navigator>
    </NavigationContainer>
  );
} ;

export default App;
